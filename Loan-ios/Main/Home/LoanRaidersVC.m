//
//  LoanRaidersVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/30.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "LoanRaidersVC.h"

@interface LoanRaidersVC ()
@property (weak, nonatomic) IBOutlet UIWebView *raidersWebView;

@end

@implementation LoanRaidersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationController.navigationBar.tintColor  = [UIColor whiteColor];
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]};
    
    [self loadBaseDataForController];
}

- (void)loadBaseDataForController {
    NSDictionary *params = @{
                             @"appId" : APP_ID,
                             @"id" : self.strID
                             };
    [SVProgressHUD showWithStatus:@"正在加载攻略..."];
    [[BeeNet sharedInstance] requestWithType:Request_GET url:INDEX_STRATEGY param:params success:^(id data) {
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            NSString *webStr = [NSString stringWithFormat:@"%@",data[@"data"]];
            [self.raidersWebView loadHTMLString:webStr baseURL:nil];
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        if(isShowLog == 1) {
            NSLog(@" -----------%@ ",data);
        }
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" INDEX_STRATEGY -------%@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
