//
//  LoanFormVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/31.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "LoanFormVC.h"
#import "HotLoanCell.h"
#import "ApplyForLoanVC.h"
#import "SignInVC.h"
#import "ShowWebViewVC.h"
@interface LoanFormVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *loanList;
@property (nonatomic, strong) NSString *didSelectLoanID;
@end

@implementation LoanFormVC

- (NSMutableArray *)loanList {
    if (!_loanList) {
        _loanList = [[NSMutableArray alloc] init];
    }
    return _loanList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = self.titleStr;
//    self.navigationController.navigationBar.tintColor  = [UIColor whiteColor];
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]};
    self.page = 1;
    [self loadTableViewLayout];
    [self loadBaseData];
}

- (void)loadBaseData {
    
    NSDictionary *params = @{
                             @"appId" : APP_ID,
                             @"pageNum" : [NSString stringWithFormat:@"%ld",self.page],
                             @"pageSize" : @"10"
                             };
    [[BeeNet sharedInstance] requestWithType:Request_GET url:INDEX_RECOMMEND param:params success:^(id data) {
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            if (self.page == 1) {
                [self.loanList removeAllObjects];
                [self.tableView.mj_header endRefreshing];
            }
            NSArray *sectionList = [NSArray arrayWithArray:data[@"data"][@"list"]];
            if (sectionList.count > 0) {
                self.page ++;
                [self.loanList addObjectsFromArray:sectionList];
                [self.tableView.mj_footer endRefreshing];
                self.page ++;
            } else {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            
            [self.tableView reloadData];
        } else {
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        if(isShowLog == 1) {
            NSLog(@" -----------%@ ",data);
        }
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" index fresh error --------%@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
    
}

#pragma mark ====================About Table View Code====================
- (void)loadTableViewLayout {
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headRefreshData)];
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadBaseData)];
    footer.stateLabel.hidden = YES;
    self.tableView.mj_footer = footer;
    
    
}

- (void)headRefreshData {
    self.page = 1;
    [self loadBaseData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.loanList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HotLoanCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loanFormCells" forIndexPath:indexPath];
    if(cell == nil) {
        cell = [[HotLoanCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"loanFormCells"];
    }
    NSDictionary *dict = self.loanList[indexPath.row];
    [cell.loanTypeImgView sd_setImageWithURL:dict[@"logo"]];
    cell.titleLab.text = dict[@"title"];
    cell.describeLab.text = dict[@"slogan"];
    cell.limit = dict[@"limit"];
    cell.rate = dict[@"monthRate"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return SCREEN_WIDTH * 90.0 / 375;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    if ([USERDEFAULTS objectForKey:@"token"]) {
//        NSDictionary *dict = self.loanList[indexPath.row];
//        self.didSelectLoanID = dict[@"id"];
//        [self performSegueWithIdentifier:@"loanFormVCToApplyVC" sender:nil];
//    } else {
//        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
//        SignInVC *vc = [board instantiateViewControllerWithIdentifier:@"signInVC"];
//        vc.isShowBack = 0;
//        vc.backBlock = ^{
//            NSDictionary *dict = self.loanList[indexPath.row];
//            self.didSelectLoanID = dict[@"id"];
//            [self performSegueWithIdentifier:@"loanFormVCToApplyVC" sender:nil];
//        };
//        [self.navigationController pushViewController:vc animated:YES];
//    }
    
    
    NSDictionary *dict = self.loanList[indexPath.row];
    
    if ([USERDEFAULTS objectForKey:@"token"]) {
        
        
        NSInteger isJump = [NSString stringWithFormat:@"%@",dict[@"templet"]].integerValue;
        if (isJump == 0) {
            
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
            ShowWebViewVC *vc = [board instantiateViewControllerWithIdentifier:@"showWebViewVC"];
            vc.urlStr = [NSString stringWithFormat:@"%@",dict[@"jumpUrl"]];
            [self.navigationController pushViewController:vc animated:YES];
            
        } else {
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
            ApplyForLoanVC *vc = [board instantiateViewControllerWithIdentifier:@"applyForLoanVC"];
            vc.idStr = dict[@"id"];
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    } else {
        
        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
        SignInVC *vc = [board instantiateViewControllerWithIdentifier:@"signInVC"];
        vc.isShowBack = 0;
        vc.backBlock = ^{
            NSDictionary *dict = self.loanList[indexPath.row];
            NSInteger isJump = [NSString stringWithFormat:@"%@",dict[@"templet"]].integerValue;
            if (isJump == 0) {
                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
                ShowWebViewVC *vc = [board instantiateViewControllerWithIdentifier:@"showWebViewVC"];
                vc.urlStr = [NSString stringWithFormat:@"%@",dict[@"jumpUrl"]];
                [self.navigationController pushViewController:vc animated:YES];
            } else {
                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
                ApplyForLoanVC *vc = [board instantiateViewControllerWithIdentifier:@"applyForLoanVC"];
                vc.idStr = dict[@"id"];
                [self.navigationController pushViewController:vc animated:YES];
            }
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
    
//    NSDictionary *dict = self.loanList[indexPath.row];
//    self.didSelectLoanID = dict[@"id"];
//    [self performSegueWithIdentifier:@"loanFormVCToApplyVC" sender:nil];
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"loanFormVCToApplyVC"]) {
        ApplyForLoanVC *vc = segue.destinationViewController;
        vc.idStr = self.didSelectLoanID;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
