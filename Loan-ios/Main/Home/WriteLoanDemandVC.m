//
//  WriteLoanDemandVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/30.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "WriteLoanDemandVC.h"
#import "LoanFormVC.h"
#import "SelectPesonalInfoVC.h"
#import "JFCityViewController.h"
@interface WriteLoanDemandVC ()
@property (weak, nonatomic) IBOutlet UITextField *moneyTF;
@property (weak, nonatomic) IBOutlet UILabel *monthCountLab;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *idCardTF;
@property (weak, nonatomic) IBOutlet UILabel *professionalIdentityLab;
@property (weak, nonatomic) IBOutlet UILabel *locatCityLab;
@property (weak, nonatomic) IBOutlet UILabel *isHasBankCardLab;
@property (weak, nonatomic) IBOutlet UILabel *usePhoneNumLab;
@property (weak, nonatomic) IBOutlet UIView *topBankgroundView;

@end

@implementation WriteLoanDemandVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationController.navigationBar.tintColor  = [UIColor whiteColor];
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]};
    self.monthCountLab.adjustsFontSizeToFitWidth = YES;

    self.topBankgroundView.layer.cornerRadius = 6;
    self.topBankgroundView.layer.masksToBounds = YES;
    self.topBankgroundView.layer.borderColor = [UIColor colorWithHexString:@"#72B4FF"].CGColor;
    self.topBankgroundView.layer.borderWidth = 1;
}

//月份选择
- (IBAction)selectMonthCountAct:(id)sender {
    [self performSegueWithIdentifier:@"loanToSelectInfoVC" sender:@"monthCount"];
}

//职业身份
- (IBAction)professionalIdentityAct:(id)sender {
    [self performSegueWithIdentifier:@"loanToSelectInfoVC" sender:@"professionalIdentity"];
}

//所在城市
- (IBAction)locatCityAct:(id)sender {
    JFCityViewController *vc = [[JFCityViewController alloc] init];
    vc.title = @"选择城市";
    [vc choseCityBlock:^(NSString *cityName) {
        self.locatCityLab.text = cityName;
    }];
    
    [self.navigationController pushViewController:vc animated:YES];
}

//信用卡
- (IBAction)isHasBankCardAct:(id)sender {
    [self performSegueWithIdentifier:@"loanToSelectInfoVC" sender:@"isHasBankCard"];
}

//手机号码
- (IBAction)usePhoneNumAct:(id)sender {
    [self performSegueWithIdentifier:@"loanToSelectInfoVC" sender:@"usePhoneNum"];
}

//去贷款
- (IBAction)goToLoanBtnAct:(id)sender {
    [self performSegueWithIdentifier:@"writeLoanDemandToLoanFormVC" sender:@"writeLoanDemandToLoanFormVC"];
}

#pragma mark ====================segue====================
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([sender isEqualToString:@"writeLoanDemandToLoanFormVC"]) {
        LoanFormVC *vc = segue.destinationViewController;
        vc.titleStr = @"贷款推荐";
    } else if ([sender isEqualToString:@"professionalIdentity"]) {
        //职业身份
        SelectPesonalInfoVC *vc = segue.destinationViewController;
        vc.selectType = 2;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.professionalIdentityLab.text = msg;
        };
    } else if ([sender isEqualToString:@"isHasBankCard"]) {
        //信用卡
        SelectPesonalInfoVC *vc = segue.destinationViewController;
        vc.selectType = 1;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.isHasBankCardLab.text = msg;
        };
    } else if ([sender isEqualToString:@"usePhoneNum"]) {
        //手机号码使用
        SelectPesonalInfoVC *vc = segue.destinationViewController;
        vc.selectType = 3;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.usePhoneNumLab.text = msg;
        };
    } else if ([sender isEqualToString:@"monthCount"]) {
        SelectPesonalInfoVC *vc = segue.destinationViewController;
        vc.selectType = 4;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.monthCountLab.text = msg;
        };
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
