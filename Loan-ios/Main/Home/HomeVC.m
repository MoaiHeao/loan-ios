//
//  HomeVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/29.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "HomeVC.h"
#import "HomeTopRunCell.h"
#import "HotLoanCell.h"
#import "HotCardCell.h"
#import "LoanFormVC.h"
#import "ShowWebViewVC.h"
#import "ApplyForLoanVC.h"
#import "SignInVC.h"
#import "LoanRaidersVC.h"

@interface HomeVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *topRunCollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHeight;
@property (weak, nonatomic) IBOutlet UILabel *raidersOneLab;
@property (weak, nonatomic) IBOutlet UILabel *raidersTwoLab;
@property (weak, nonatomic) IBOutlet UITableView *hotLoanTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loanViewHeight;
@property (weak, nonatomic) IBOutlet UITableView *hotCardTableView;
@property (weak, nonatomic) IBOutlet UIView *hotCardView;
@property (weak, nonatomic) IBOutlet UIView *hotLoanView;
@property (weak, nonatomic) IBOutlet UIView *loanTitleView;
@property (weak, nonatomic) IBOutlet UIView *cardTitleView;
@property (weak, nonatomic) IBOutlet UIView *raidersoneView;
@property (weak, nonatomic) IBOutlet UIView *raidersTwoView;


@property (nonatomic, assign) NSInteger timeNum;

@property (nonatomic, strong) NSArray *topRunDataList;
@property (nonatomic, strong) NSArray *raidersList;
@property (nonatomic, strong) NSArray *loanList;
@property (nonatomic, strong) NSArray *cardList;

@property (nonatomic, strong) NSString *didSelectURL;
@property (nonatomic, strong) NSString *didSelectRaiderID;
@property (nonatomic, strong) NSString *didSelectRaiderTwoID;

@property (nonatomic, strong) NSTimer *timer;



@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    
    self.raidersoneView.layer.cornerRadius = 3;
    self.raidersoneView.layer.masksToBounds = YES;
    self.raidersoneView.layer.borderColor = [UIColor colorWithHexString:@"#FF6D7F"].CGColor;
    self.raidersoneView.layer.borderWidth = 1;
    self.raidersTwoView.layer.cornerRadius = 3;
    self.raidersTwoView.layer.masksToBounds = YES;
    self.raidersTwoView.layer.borderColor = [UIColor colorWithHexString:@"#FF6D7F"].CGColor;
    self.raidersTwoView.layer.borderWidth = 1;
    
    self.navigationController.delegate = self;
    
    [self loadTopRunCollectionViewLayout];
    [self loadHotLoanTableViewLayout];
    [self loadBasieData];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    
    self.hotCardView.hidden = ![USERDEFAULTS boolForKey:@"isHaveCardMode"];
    
    [USERDEFAULTS setInteger:0 forKey:@"tabtar"];
    [USERDEFAULTS synchronize];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:YES];
    //显示启动页
    BOOL isFirst = [USERDEFAULTS boolForKey:@"isFirstOpen"];
//   [self performSegueWithIdentifier:@"showLaunchScreen" sender:nil];
    if (!isFirst) {

        [self performSegueWithIdentifier:@"showLaunchScreen" sender:nil];

    }
    
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:viewController == self animated:YES];
}

- (void)loadBasieData {
    
    NSDictionary *params = @{
                             @"appId" : APP_ID
                             };
    [[BeeNet sharedInstance] requestWithType:Request_GET url:INDEX_INDEX param:params success:^(id data) {
        
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            
            self.topRunDataList = [NSArray arrayWithArray:data[@"data"][@"banner"]];
            if (self.topRunDataList.count > 0) {
                [self.topRunCollectionView reloadData];
                self.pageControl.numberOfPages = self.topRunDataList.count;
                [self preTimeRun];
            }
            self.raidersList = [NSArray arrayWithArray:data[@"data"][@"strategy"]];
            if (self.raidersList.count > 0) {
                [self setDataForRaiders];
            }
            self.loanList = [NSArray arrayWithArray:data[@"data"][@"company"]];
            if (self.loanList.count > 0) {
                [self.hotLoanTableView reloadData];
            }
            self.cardList = [NSArray arrayWithArray:data[@"data"][@"creditCard"]];
            if (self.cardList.count > 0) {
                [self.hotCardTableView reloadData];
            } 
            
        } else {
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        if(isShowLog == 1) {
            NSLog(@" ------%@ ",data);
            NSLog(@"topRunDataList-----%@",self.topRunDataList);
            NSLog(@"raidersList-----%@",self.raidersList);
            NSLog(@"loanList-----%@",self.loanList);
            NSLog(@"cardList-----%@",self.cardList);
        }
        
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" index index error %@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
    
}

- (void)setDataForRaiders {
    
    self.didSelectRaiderID = self.raidersList.firstObject[@"id"];
    if (self.raidersList.count > 1) {
        self.raidersOneLab.text = [NSString stringWithFormat:@"%@",self.raidersList[0][@"title"]];
        self.didSelectRaiderID = self.raidersList[0][@"id"];
        self.raidersTwoLab.text = [NSString stringWithFormat:@"%@",self.raidersList[1][@"title"]];
        self.didSelectRaiderTwoID = self.raidersList[1][@"id"];
    } else {
        self.raidersOneLab.text = [NSString stringWithFormat:@"%@",self.raidersList.firstObject[@"title"]];
        self.didSelectRaiderID = self.raidersList[0][@"id"];
        self.raidersTwoLab.text = [NSString stringWithFormat:@"%@",self.raidersList.firstObject[@"title"]];
        self.didSelectRaiderTwoID = self.raidersList[0][@"id"];
    }
}


//贷款推荐
- (IBAction)loanRecommendAct:(id)sender {
    
        [self performSegueWithIdentifier:@"homeToWriteLoanDemandVC" sender:nil];
    
}

//新品专区
- (IBAction)newProductAct:(id)sender {
    [self performSegueWithIdentifier:@"homeToLoanFormVC" sender:nil];
}



#pragma mark ====================About Top Run Collection View====================
- (void)loadTopRunCollectionViewLayout {
    
    self.topRunCollectionView.delegate = self;
    self.topRunCollectionView.dataSource = self;
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.topRunCollectionView.collectionViewLayout = flowLayout;
    [self.topRunCollectionView setPagingEnabled:YES];
    self.topRunCollectionView.scrollEnabled = YES;
    
    self.timeNum = 1;
    
    
}

- (void)preTimeRun {
    self.timer = [NSTimer timerWithTimeInterval:5.0 target:self selector:@selector(timeChage) userInfo:nil repeats:YES];
    NSRunLoop *runloop = [NSRunLoop currentRunLoop];
    [runloop addTimer:self.timer forMode:NSDefaultRunLoopMode];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.topRunDataList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HomeTopRunCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"homeRunTopCells" forIndexPath:indexPath];
    NSDictionary *dict = self.topRunDataList[indexPath.row];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:dict[@"imageUrl"]]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(SCREEN_WIDTH , SCREEN_WIDTH * 176.0 / 375.0);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = self.topRunDataList[indexPath.row];
    self.didSelectURL = dict[@"jumpUrl"];
    if ([USERDEFAULTS objectForKey:@"token"]) {
//        NSDictionary *dict = self.topRunDataList[indexPath.row];
//        self.didSelectURL = dict[@"jumpUrl"];
        [self performSegueWithIdentifier:@"showHomeWebView" sender:nil];
    } else {
        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
        SignInVC *vc = [board instantiateViewControllerWithIdentifier:@"signInVC"];
        vc.isShowBack = 0;
        vc.backBlock = ^{
            [self performSegueWithIdentifier:@"showHomeWebView" sender:nil];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(scrollView == self.topRunCollectionView) {
        CGFloat scrollX = self.topRunCollectionView.contentOffset.x + 10;
        int pageNum = scrollX / SCREEN_WIDTH;
        self.pageControl.currentPage = pageNum;
    }
}

- (void)timeChage {
    //变换轮播图
    NSInteger page = self.timeNum % self.topRunDataList.count;
//    CGFloat offSetX = page * SCREEN_WIDTH;
    [UIView animateWithDuration:0.5 animations:^{
//        self.topRunCollectionView.contentOffset = CGPointMake(offSetX, 0);
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:page inSection:0];
        [self.topRunCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }];
    
    //变换策略
    if (self.raidersList.count > 1) {
        NSInteger raiderNum = self.timeNum % self.raidersList.count;
        [UIView animateWithDuration:0.5 animations:^{
            self.raidersOneLab.text = [NSString stringWithFormat:@"%@",self.raidersList[raiderNum][@"title"]];
            self.didSelectRaiderID = self.raidersList[raiderNum][@"id"];
            if (raiderNum + 2 > self.raidersList.count) {
                self.raidersTwoLab.text = [NSString stringWithFormat:@"%@",self.raidersList[0][@"title"]];
                self.didSelectRaiderTwoID = self.raidersList[0][@"id"];
            } else {
                self.raidersTwoLab.text = [NSString stringWithFormat:@"%@",self.raidersList[raiderNum + 1][@"title"]];
                self.didSelectRaiderTwoID = self.raidersList[raiderNum + 1][@"id"];
            }
        }];
    }
    
    self.timeNum ++;
}

- (IBAction)raidersOneAct:(id)sender {
    
    [self performSegueWithIdentifier:@"homeToRaidersVC" sender:self.didSelectRaiderID];
    
}

- (IBAction)radersTwoAct:(id)sender {
    
    [self performSegueWithIdentifier:@"homeToRaidersVC" sender:self.didSelectRaiderTwoID];
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView == self.topRunCollectionView) {
        [self.timer invalidate];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if (scrollView == self.topRunCollectionView) {
        NSInteger index = scrollView.contentOffset.x / scrollView.frame.size.width;
        self.timeNum = index;
        [self preTimeRun];
    }
}



#pragma mark ====================About Table View Code====================
// Hot Loan Table View
- (void)loadHotLoanTableViewLayout {
    
    self.hotLoanTableView.delegate = self;
    self.hotLoanTableView.dataSource = self;
    
    self.hotCardTableView.delegate = self;
    self.hotCardTableView.dataSource = self;
    
    self.hotLoanTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.hotCardTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == self.hotLoanTableView) {
        return self.loanList.count;
    } else {
        return self.cardList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == self.hotLoanTableView) {
        NSDictionary *dict = self.loanList[indexPath.row];
        HotLoanCell *cell = [tableView dequeueReusableCellWithIdentifier:@"hotLoanCells" forIndexPath:indexPath];
        if(cell == nil) {
            cell = [[HotLoanCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"hotLoanCells"];
        }
        [cell.loanTypeImgView sd_setImageWithURL:[NSURL URLWithString:dict[@"logo"]]];
        cell.titleLab.text = dict[@"title"];
        cell.describeLab.text = dict[@"slogan"];
        cell.limit = dict[@"limit"];
        cell.rate = dict[@"monthRate"];
        return cell;
    } else {
        NSDictionary *dict = self.cardList[indexPath.row];
        HotCardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"hotCardCells" forIndexPath:indexPath];
        if(cell == nil) {
            cell = [[HotCardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"hotCardCells"];
        }
        [cell.cardImgView sd_setImageWithURL:[NSURL URLWithString:dict[@"logo"]]];
        cell.titleLab.text = dict[@"name"];
        cell.markStr = dict[@"tag"];
        cell.sonNumLab.text = dict[@"num"];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == self.hotLoanTableView) {
        return SCREEN_WIDTH * 88.0 / 375;
    } else {
        return SCREEN_WIDTH * 88.0 / 375;
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.hotCardView.hidden) {
        if (indexPath.row == tableView.indexPathsForVisibleRows.lastObject.row) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.contentHeight.constant = self.hotLoanView.frame.origin.y + self.loanTitleView.frame.size.height + self.hotLoanTableView.contentSize.height +  self.topRunCollectionView.frame.size.height + 8 ;
            });
        }
    } else {
        if(indexPath.row == tableView.indexPathsForVisibleRows.lastObject.row) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.loanViewHeight.constant = self.hotCardTableView.contentSize.height + self.cardTitleView.frame.size.height + 8;
                self.contentHeight.constant = self.hotLoanView.frame.origin.y + self.loanTitleView.frame.size.height + self.hotLoanTableView.contentSize.height + self.hotCardTableView.contentSize.height + self.cardTitleView.frame.size.height - self.topRunCollectionView.frame.size.height + 8 ;
                
            });
        }
    }
//
//    if(tableView == self.hotCardTableView) {
//
//    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == self.hotLoanTableView) {
        NSDictionary *dict = self.loanList[indexPath.row];
        
        NSInteger isJump = [NSString stringWithFormat:@"%@",dict[@"templet"]].integerValue;
        if (isJump == 0) {
            self.didSelectURL = dict[@"jumpUrl"];
            if ([USERDEFAULTS objectForKey:@"token"]) {
                [self performSegueWithIdentifier:@"showHomeWebView" sender:nil];
            } else {
                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
                SignInVC *vc = [board instantiateViewControllerWithIdentifier:@"signInVC"];
                vc.isShowBack = 0;
                vc.backBlock = ^{
                    self.didSelectURL = dict[@"jumpUrl"];
                    [self performSegueWithIdentifier:@"showHomeWebView" sender:nil];
                };
                [self.navigationController pushViewController:vc animated:YES];
            }
//            [self performSegueWithIdentifier:@"showHomeWebView" sender:nil];
            
        } else {
            
            if ([USERDEFAULTS objectForKey:@"token"]) {
                self.didSelectURL = dict[@"id"];
                [self performSegueWithIdentifier:@"homeToApplyForLoanVC" sender:nil];
            } else {
                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
                SignInVC *vc = [board instantiateViewControllerWithIdentifier:@"signInVC"];
                vc.isShowBack = 0;
                vc.backBlock = ^{
                    self.didSelectURL = dict[@"id"];
                    [self performSegueWithIdentifier:@"homeToApplyForLoanVC" sender:nil];
                };
                [self.navigationController pushViewController:vc animated:YES];
            }
            
        }
        
    } else {
    
        if ([USERDEFAULTS objectForKey:@"token"]) {
            NSDictionary *dict = self.cardList[indexPath.row];
            self.didSelectURL = dict[@"jumpUrl"];
            [self performSegueWithIdentifier:@"showHomeWebView" sender:nil];

        } else {
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
            SignInVC *vc = [board instantiateViewControllerWithIdentifier:@"signInVC"];
            vc.isShowBack = 0;
            vc.backBlock = ^{
                NSDictionary *dict = self.cardList[indexPath.row];
                self.didSelectURL = dict[@"jumpUrl"];
                [self performSegueWithIdentifier:@"showHomeWebView" sender:nil];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }       
        
    }
}

#pragma mark ====================Segue====================
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"homeToLoanFormVC"]) {
        LoanFormVC *vc = segue.destinationViewController;
        vc.titleStr = @"新品专区";
    } else if ([segue.identifier isEqualToString:@"showHomeWebView"]) {
        
        ShowWebViewVC *vc = segue.destinationViewController;
        vc.urlStr = self.didSelectURL;
        
    } else if ([segue.identifier isEqualToString:@"homeToApplyForLoanVC"]) {
        
        ApplyForLoanVC *vc = segue.destinationViewController;
        vc.idStr = self.didSelectURL;
        
        
    } else if ([segue.identifier isEqualToString:@"homeToRaidersVC"]) {
        LoanRaidersVC *vc = segue.destinationViewController;
        vc.strID = sender;
    }
}

- (void)dealloc {
    [self.timer invalidate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
