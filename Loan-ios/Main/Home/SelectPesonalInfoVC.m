//
//  SelectPesonalInfoVC.m
//  Loan-ios
//
//  Created by Hao on 2018/11/5.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "SelectPesonalInfoVC.h"
#import "SelectMessageDetailCell.h"
@interface SelectPesonalInfoVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHeight;


@property (nonatomic, strong) NSArray *typeList;

@property (nonatomic, strong) NSArray *yesOrNoList;

@property (nonatomic, strong) NSArray *professionalIdentityList;

@property (nonatomic, strong) NSArray *phoneUseTimeList;

@property (nonatomic, strong) NSArray *monthCountList;

@end
//selectPensonlInfolCell
@implementation SelectPesonalInfoVC

//是否
- (NSArray *)yesOrNoList {
    if(!_yesOrNoList) {
        _yesOrNoList = @[@"是", @"否"];
    }
    return _yesOrNoList;
}

//职业身份
- (NSArray *)professionalIdentityList {
    if(!_professionalIdentityList) {
        _professionalIdentityList = @[@"上班族", @"个体户", @"企业主", @"自由职业"];
    }
    return _professionalIdentityList;
}

//手机使用时间
- (NSArray *)phoneUseTimeList {
    if (!_phoneUseTimeList) {
        _phoneUseTimeList = @[@"1-5个月", @"6个月以上"];
    }
    return _phoneUseTimeList;
}

//贷款期限
- (NSArray *)monthCountList {
    if (!_monthCountList) {
        _monthCountList = @[@"7-15天", @"1个月", @"3个月"];
    }
    return _monthCountList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self slidingReturnToPreviousPage];
    
    switch (self.selectType) {
        case 1 : {
            self.typeList = [NSArray arrayWithArray:self.yesOrNoList];
            break;
        }
        case 2 : {
            self.typeList = [NSArray arrayWithArray:self.professionalIdentityList];
            break;
        } case 3 : {
            self.typeList = [NSArray arrayWithArray:self.phoneUseTimeList];
            break;
        } case 4 : {
            self.typeList = [NSArray arrayWithArray:self.monthCountList];
            break;
        }
            
            
        default:
            break;
    }
    
}

- (IBAction)backAct:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)slidingReturnToPreviousPage {
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(returnPreviousPage:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipe];
}

- (void)returnPreviousPage:(UISwipeGestureRecognizer *)swipe {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark ====================About Table View Code====================
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.typeList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *contentStr = self.typeList[indexPath.row];
    SelectMessageDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"selectPensonlInfolCell" forIndexPath:indexPath];
    if(!cell) {
        cell = [[SelectMessageDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"selectPensonlInfolCell"];
    }
    cell.selectDetailLab.text = contentStr;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return SCREEN_WIDTH * 40.0 / 375;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == tableView.indexPathsForVisibleRows.lastObject.row) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.contentHeight.constant = self.tableView.contentSize.height;
        });
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *didseleteStr = self.typeList[indexPath.row];
    self.didseleInfoIs(didseleteStr);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
