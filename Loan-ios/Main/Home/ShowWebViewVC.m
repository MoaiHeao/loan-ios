//
//  ShowWebViewVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/29.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "ShowWebViewVC.h"
#import "SignInVC.h"
@interface ShowWebViewVC ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation ShowWebViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationController.navigationBar.tintColor  = [UIColor whiteColor];
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],
//    NSFontAttributeName:[UIFont systemFontOfSize:16]};
    
    self.webView.delegate = self;
    
}

- (void)viewWillAppear:(BOOL)animated {
 
    [super viewWillAppear:YES];
//    if ([USERDEFAULTS objectForKey:@"token"]) {
//        [SVProgressHUD showWithStatus:@"加载中..."];
//        if (self.urlStr.length == 0) {
//            self.urlStr = @"http://www.baidu.com";
//        }
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.urlStr]];
//        [self.webView loadRequest:request];
//    } else {
//        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
//        SignInVC *vc = [board instantiateViewControllerWithIdentifier:@"signInVC"];
//        vc.isShowBack = 0;
//        vc.backBlock = ^{
//            [SVProgressHUD showWithStatus:@"加载中..."];
//            if (self.urlStr.length == 0) {
//                self.urlStr = @"http://www.baidu.com";
//            }
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.urlStr]];
//            [self.webView loadRequest:request];
//        };
//        [self.navigationController pushViewController:vc animated:YES];
//    }
    
    
    
    [SVProgressHUD showWithStatus:@"加载中..."];
    if (self.urlStr.length == 0) {
        self.urlStr = @"http://www.baidu.com";
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.urlStr]];
    [self.webView loadRequest:request];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    [SVProgressHUD dismiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [SVProgressHUD showErrorWithStatus:@"网络连接出错!"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
