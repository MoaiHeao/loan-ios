//
//  HomeTopRunCell.h
//  Loan-ios
//
//  Created by Hao on 2018/10/29.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTopRunCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@end
