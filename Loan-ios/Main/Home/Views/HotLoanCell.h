//
//  HotLoanCell.h
//  Loan-ios
//
//  Created by Hao on 2018/10/30.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotLoanCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *loanTypeImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *describeLab;
@property (weak, nonatomic) IBOutlet UILabel *amountLab;
@property (weak, nonatomic) IBOutlet UIView *moneyBoardView;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;
@property (weak, nonatomic) IBOutlet UILabel *pensentLab;
@property (weak, nonatomic) IBOutlet UILabel *interestRateLab;

@property (nonatomic, strong) NSString *limit;
@property (nonatomic, strong) NSString *rate;

@end
