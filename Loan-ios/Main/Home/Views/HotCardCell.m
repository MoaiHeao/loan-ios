//
//  HotCardCell.m
//  Loan-ios
//
//  Created by Hao on 2018/10/30.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "HotCardCell.h"

@implementation HotCardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    self.cardImgView.layer.cornerRadius = 8;
    self.cardImgView.layer.masksToBounds = YES;
    
    self.boardOneView.layer.cornerRadius = self.boardOneView.frame.size.height / 2;
    self.boardOneView.layer.masksToBounds = YES;
    self.boardOneView.layer.borderColor = [[UIColor colorWithHexString:@"#72B4FF"] CGColor];
    self.boardOneView.layer.borderWidth = 1;

    self.boardTwoView.layer.cornerRadius = self.boardOneView.frame.size.height / 2;
    self.boardTwoView.layer.masksToBounds = YES;
    self.boardTwoView.layer.borderColor = [[UIColor colorWithHexString:@"#72B4FF"] CGColor];
    self.boardTwoView.layer.borderWidth = 1;
    
    
}

- (void)setMarkStr:(NSString *)markStr {
    _markStr = markStr;
    NSArray *strArr = [markStr componentsSeparatedByString:@","];
    self.markOneLab.text = [NSString stringWithFormat:@"%@",strArr.firstObject];
    self.markTwoLab.text = [NSString stringWithFormat:@"%@",strArr.lastObject];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
