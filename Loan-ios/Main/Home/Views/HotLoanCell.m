//
//  HotLoanCell.m
//  Loan-ios
//
//  Created by Hao on 2018/10/30.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "HotLoanCell.h"

@implementation HotLoanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.loanTypeImgView.layer.cornerRadius = 8;
    self.loanTypeImgView.layer.masksToBounds = YES;
    
    self.moneyBoardView.layer.cornerRadius = self.moneyBoardView.frame.size.height / 2;
    self.moneyBoardView.layer.masksToBounds = YES;
    self.moneyBoardView.layer.borderColor = [[UIColor colorWithHexString:@"#FF6D7F"] CGColor];
    self.moneyBoardView.layer.borderWidth = 1;
    
}

- (void)setRate:(NSString *)rate {
    _rate = rate;
    self.pensentLab.text = [NSString stringWithFormat:@"%@%%",rate];
}

- (void)setLimit:(NSString *)limit {
    _limit = limit;
    NSArray *moneyArr = [limit componentsSeparatedByString:@","];
    self.moneyLab.text = [NSString stringWithFormat:@"%@-%@元",moneyArr.firstObject,moneyArr.lastObject];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
