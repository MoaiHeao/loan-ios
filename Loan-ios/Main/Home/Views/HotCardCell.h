//
//  HotCardCell.h
//  Loan-ios
//
//  Created by Hao on 2018/10/30.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotCardCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cardImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIView *boardOneView;
@property (weak, nonatomic) IBOutlet UILabel *markOneLab;
@property (weak, nonatomic) IBOutlet UIView *boardTwoView;
@property (weak, nonatomic) IBOutlet UILabel *markTwoLab;
@property (weak, nonatomic) IBOutlet UILabel *sonNumLab;

@property (nonatomic, strong) NSString *markStr;

@end
