//
//  ViewController.m
//  Loan-ios
//
//  Created by Hao on 2018/10/29.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "ViewController.h"
#import "SignInVC.h"
@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *startPageScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *oneimgView;
@property (weak, nonatomic) IBOutlet UIImageView *twoImgView;
@property (weak, nonatomic) IBOutlet UIImageView *threeImgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeight;


@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, strong) NSTimer *runTimer;

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.oneimgView setImage:[UIImage  imageNamed:@"start1"]];
    [self.twoImgView setImage:[UIImage  imageNamed:@"start3"]];
    [self.threeImgView setImage:[UIImage  imageNamed:@"start2"]];

    [USERDEFAULTS setBool:YES forKey:@"isFirstOpen"];
    [USERDEFAULTS synchronize];
    
//    self.navigationController.delegate = self;
    
    self.startPageScrollView.scrollEnabled = YES;
//    sel.startPageScrollView.
    self.startPageScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * 3, 0);
    
    self.buttonHeight.constant = (SCREEN_HEIGHT * (90.0 / 736));
    
}



- (IBAction)nextBtnAct:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
