//
//  ApplyForLoanVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/31.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "ApplyForLoanVC.h"
#import "ShowWebViewVC.h"
#import "SelectPesonalInfoVC.h"
#import "SignInVC.h"
@interface ApplyForLoanVC ()
@property (weak, nonatomic) IBOutlet UIImageView *loanTypeIconImgView;
@property (weak, nonatomic) IBOutlet UILabel *loanTypeTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *repaymentLab;
@property (weak, nonatomic) IBOutlet UILabel *interestLab;
@property (weak, nonatomic) IBOutlet UILabel *rateLab;
@property (weak, nonatomic) IBOutlet UILabel *penpleNumberLab;
@property (weak, nonatomic) IBOutlet UILabel *reteRangeLab;
@property (weak, nonatomic) IBOutlet UILabel *loanRangeLab;
@property (weak, nonatomic) IBOutlet UILabel *timeRangeLab;
@property (weak, nonatomic) IBOutlet UILabel *monthCountLab;
@property (weak, nonatomic) IBOutlet UITextField *loanNumberTF;

@property (weak, nonatomic) IBOutlet UILabel *specificationLab;
@property (weak, nonatomic) IBOutlet UILabel *cycleLab;
@property (weak, nonatomic) IBOutlet UILabel *loanDateLab;
@property (weak, nonatomic) IBOutlet UILabel *repamentMethodLab;
@property (weak, nonatomic) IBOutlet UILabel *introduceLab;
@property (weak, nonatomic) IBOutlet UIView *topBackgroundView;



@property (nonatomic, strong) NSDictionary *dataDict;

@property (nonatomic, strong) NSString *monthRate;

@end

@implementation ApplyForLoanVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationController.navigationBar.tintColor  = [UIColor whiteColor];
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]};
    self.loanRangeLab.adjustsFontSizeToFitWidth = YES;
    self.monthCountLab.adjustsFontSizeToFitWidth = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loanMoneyChange:) name:UITextFieldTextDidChangeNotification object:self.loanNumberTF];
    
    self.topBackgroundView.layer.cornerRadius = 6;
    self.topBackgroundView.layer.masksToBounds = YES;
    self.topBackgroundView.layer.borderColor = [UIColor colorWithHexString:@"#72B4FF"].CGColor;
    self.topBackgroundView.layer.borderWidth = 1;
    
    [self loadBaseData];
    
}

//- (void)viewWillAppear:(BOOL)animated {
//
//
//    [super viewWillAppear:YES];
//
//    if ([USERDEFAULTS objectForKey:@"token"]) {
//
//    } else {
//        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
//        SignInVC *vc = [board instantiateViewControllerWithIdentifier:@"signInVC"];
//        vc.isShowBack = 1;
//        vc.backBlock = ^{
//            [self loadBaseData];
//        };
//        [self.navigationController pushViewController:vc animated:YES];
//    }
//}

- (void)loanMoneyChange:(NSNotification *)notification {
    
    UITextField *tf = notification.object;
    [self calculationData:self.monthCountLab.text andLoanMoney:tf.text];
    
}

- (void)loadBaseData {
    
    NSDictionary *params = @{
                             @"appId" : APP_ID,
                             @"id" : self.idStr
                             };
    [[BeeNet sharedInstance] requestWithType:Request_GET url:INDEX_COMPANY param:params success:^(id data) {
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            self.dataDict = [NSDictionary dictionaryWithDictionary:data[@"data"]];
            [self setBaseDataForController];
        } else {
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        if(isShowLog == 1) {
            NSLog(@" --------------%@ ",self.dataDict);
        }
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" index company error -----%@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
    
}

- (void)setBaseDataForController {
    
    
    
    [self.loanTypeIconImgView sd_setImageWithURL:self.dataDict[@"logo"]];
    self.loanTypeTitleLab.text = self.dataDict[@"title"];
    
    self.rateLab.text = [NSString stringWithFormat:@"%@%%",self.dataDict[@"monthRate"]];
    self.monthRate = self.dataDict[@"monthRate"];
    [self calculationData:self.monthCountLab.text andLoanMoney:self.loanNumberTF.text];
    self.penpleNumberLab.text = [NSString stringWithFormat:@"%@人",self.dataDict[@"applicationNum"]];
    
    NSArray *rateArr = [self.dataDict[@"interestRate"] componentsSeparatedByString:@","];
    self.reteRangeLab.text = [NSString stringWithFormat:@"%@%%-%@%%",rateArr.firstObject,rateArr.lastObject];
    
    NSArray *loanArr = [self.dataDict[@"loan"] componentsSeparatedByString:@","];
    self.loanRangeLab.text = [NSString stringWithFormat:@"%@-%@元",loanArr.firstObject,loanArr.lastObject];
    
    NSArray *timeArr = [self.dataDict[@"deadline"] componentsSeparatedByString:@","];
    self.timeRangeLab.text = [NSString stringWithFormat:@"%@-%@个月",timeArr.firstObject,timeArr.lastObject];
    
    NSArray *auditArr = [self.dataDict[@"audit"] componentsSeparatedByString:@","];
    self.specificationLab.text = [NSString stringWithFormat:@"审核说明:%@",auditArr[0]];
    self.cycleLab.text = [NSString stringWithFormat:@"审核周期:%@",auditArr[1]];
    self.loanDateLab.text = [NSString stringWithFormat:@"放款日期:%@",auditArr[2]];
    self.repamentMethodLab.text = [NSString stringWithFormat:@"还款方式:%@",auditArr[3]];
    
    self.introduceLab.text = self.dataDict[@"introduction"];
    
}



- (void)calculationData:(NSString *)monthCount andLoanMoney:(NSString *)money {
    CGFloat loanMoney = money.floatValue;
    CGFloat rate = self.monthRate.floatValue / 100;
    CGFloat month;
    CGFloat totalRate;
    CGFloat monthPay;
    if ([monthCount containsString:@"个"]) {
        NSArray *strArr = [monthCount componentsSeparatedByString:@"个"];
        month = [NSString stringWithFormat:@"%@",strArr.firstObject].floatValue;
        totalRate = loanMoney * month * rate;
        monthPay = (loanMoney + totalRate) / month;
    } else {
        month = 0.5;
        totalRate = loanMoney * month * rate;
        monthPay = (loanMoney + totalRate) / 1;
    }
    
    
    
    self.interestLab.text = [NSString stringWithFormat:@"%0.1f元",totalRate];
    self.repaymentLab.text = [NSString stringWithFormat:@"%0.1f元",monthPay];
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.loanTypeIconImgView.layer.cornerRadius = self.loanTypeIconImgView.frame.size.height / 2;
    self.loanTypeIconImgView.layer.masksToBounds = YES;
}

- (IBAction)showAlatAct:(id)sender {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"展示利率仅供参考,实际利率以各家贷款页面展示为准" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"我知道了" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}



- (IBAction)applyImmediatelyBtnAct:(id)sender {
    
//    if ([USERDEFAULTS objectForKey:@"token"]) {
//        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
//        ShowWebViewVC *vc = [board instantiateViewControllerWithIdentifier:@"showWebViewVC"];
//        vc.urlStr = self.dataDict[@"jumpUrl"];
//        [self.navigationController pushViewController:vc animated:YES];
//    } else {
//        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
//        SignInVC *vc = [board instantiateViewControllerWithIdentifier:@"signInVC"];
//        vc.isShowBack = 0;
//        vc.backBlock = ^{
//            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
//            ShowWebViewVC *vc = [board instantiateViewControllerWithIdentifier:@"showWebViewVC"];
//            vc.urlStr = self.dataDict[@"jumpUrl"];
//            [self.navigationController pushViewController:vc animated:YES];
//        };
//        [self.navigationController pushViewController:vc animated:YES];
//    }
    
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
    ShowWebViewVC *vc = [board instantiateViewControllerWithIdentifier:@"showWebViewVC"];
    vc.urlStr = self.dataDict[@"jumpUrl"];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}



//selectMonthCounVC
- (IBAction)selectMonthCountAct:(id)sender {
    [self performSegueWithIdentifier:@"selectMonthCounVC" sender:@"monthCount"];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([sender isEqualToString:@"monthCount"]) {
        SelectPesonalInfoVC *vc = segue.destinationViewController;
        vc.selectType = 4;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.monthCountLab.text = msg;
            [self calculationData:msg andLoanMoney:self.loanNumberTF.text];
        };
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
