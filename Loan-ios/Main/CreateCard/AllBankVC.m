//
//  AllBankVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/30.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "AllBankVC.h"
#import "AllBankCardCell.h"
#import "ShowWebViewVC.h"
#import "SignInVC.h"
@interface AllBankVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *bankList;
@property (nonatomic, assign) NSInteger page;
@end

@implementation AllBankVC

- (NSMutableArray *)bankList {
    if (!_bankList) {
        _bankList = [[NSMutableArray alloc] init];
    }
    return _bankList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationController.navigationBar.tintColor  = [UIColor whiteColor];
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]};
    
    [self loadTableViewLayout];
    self.page = 1;
 
    [self loadBaseDataForController];
}

- (void)loadBaseDataForController {
    
    NSDictionary *params = @{
                             @"appId" : APP_ID,
                             @"pageNum" : [NSString stringWithFormat:@"%ld",self.page],
                             @"pageSize" : @"10"
                             };
    [[BeeNet sharedInstance] requestWithType:Request_GET url:CREDIT_BANK_LIST param:params success:^(id data) {
        
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            NSArray *sectionArr = [NSArray arrayWithArray:data[@"data"][@"list"]];
            if (self.page == 1) {
                [self.tableView.mj_header endRefreshing];
                [self.bankList removeAllObjects];
            }
            if (sectionArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                
            } else {
                [self.tableView.mj_footer endRefreshing];
                [self.bankList addObjectsFromArray:sectionArr];
                [self.tableView reloadData];
                self.page ++;
            }
            
            if(isShowLog == 1) {
                NSLog(@" ------------%@ ",data);
            }
        } else {
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" credit bank list error ------%@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
    
}

#pragma mark ====================About Table View Code====================

- (void)loadTableViewLayout {
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headRefreshData)];
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadBaseDataForController)];
    footer.stateLabel.hidden = YES;
    self.tableView.mj_footer = footer;
    
}

- (void)headRefreshData {
 
    self.page = 1;
    [self loadBaseDataForController];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.bankList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AllBankCardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"allBankCells" forIndexPath:indexPath];
    if(cell == nil) {
        cell = [[AllBankCardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"allBankCells"];
    }
    NSDictionary *dict = self.bankList[indexPath.row];
    [cell.bankIconImgView sd_setImageWithURL:dict[@"logo"]];
    cell.bankNameLab.text = dict[@"name"];
    cell.detailLab.text = dict[@"slogan"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return SCREEN_WIDTH * 80 / 375;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if ([USERDEFAULTS objectForKey:@"token"]) {
        NSDictionary *dict = self.bankList[indexPath.row];
        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
        ShowWebViewVC *vc = [board instantiateViewControllerWithIdentifier:@"showWebViewVC"];
        vc.urlStr = dict[@"jumpUrl"];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
        SignInVC *vc = [board instantiateViewControllerWithIdentifier:@"signInVC"];
        vc.isShowBack = 0;
        vc.backBlock = ^{
            NSDictionary *dict = self.bankList[indexPath.row];
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
            ShowWebViewVC *vc = [board instantiateViewControllerWithIdentifier:@"showWebViewVC"];
            vc.urlStr = dict[@"jumpUrl"];
            [self.navigationController pushViewController:vc animated:YES];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
