//
//  AllBankCardCell.m
//  Loan-ios
//
//  Created by Hao on 2018/10/30.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "AllBankCardCell.h"

@implementation AllBankCardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.bankIconImgView.layer.cornerRadius = 8;
    self.bankIconImgView.layer.masksToBounds = YES;

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
