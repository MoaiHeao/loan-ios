//
//  AllBankCardCell.h
//  Loan-ios
//
//  Created by Hao on 2018/10/30.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllBankCardCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bankIconImgView;
@property (weak, nonatomic) IBOutlet UILabel *bankNameLab;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;

@end
