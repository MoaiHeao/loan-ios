//
//  CreateCardVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/30.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "CreateCardVC.h"
#import "HotCardCell.h"
#import "ShowWebViewVC.h"
#import "SignInVC.h"
@interface CreateCardVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHeight;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIImageView *oneImgView;
@property (weak, nonatomic) IBOutlet UILabel *oneBankNameLan;
@property (weak, nonatomic) IBOutlet UILabel *oneBankSloganLab;

@property (weak, nonatomic) IBOutlet UIImageView *twoImgView;
@property (weak, nonatomic) IBOutlet UILabel *twoBankNameLab;
@property (weak, nonatomic) IBOutlet UILabel *twoSloganLab;

@property (weak, nonatomic) IBOutlet UIImageView *threeImgView;
@property (weak, nonatomic) IBOutlet UILabel *threeBankLab;
@property (weak, nonatomic) IBOutlet UILabel *threeSloganLab;
@property (weak, nonatomic) IBOutlet UIView *twoView;
@property (weak, nonatomic) IBOutlet UIView *threeView;
@property (weak, nonatomic) IBOutlet UIView *fureView;
@property (weak, nonatomic) IBOutlet UIView *bigView;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeight;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heghtMagim;



@property (nonatomic, strong) NSArray *topBankList;
@property (nonatomic, strong) NSMutableArray *cardList;

@property (nonatomic, assign) NSInteger page;

@end

@implementation CreateCardVC

- (NSMutableArray *)cardList {
 
    if (!_cardList) {
        _cardList = [[NSMutableArray alloc] init];
    }
    return _cardList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]};
 //
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.oneBankSloganLab.adjustsFontSizeToFitWidth = YES;
    self.twoSloganLab.adjustsFontSizeToFitWidth = YES;
    self.threeSloganLab.adjustsFontSizeToFitWidth = YES;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadBaseDataForController)];
    footer.stateLabel.hidden = YES;
    self.tableView.mj_footer = footer;
    
    self.page = 1;
//    self.topWidth.constant = SCREEN_WIDTH ;
    self.heghtMagim.constant = SCREEN_WIDTH * 166 / 375;
     NSLog(@"========oldheighhyyy==========%lf",self.contentHeight.constant);
    [self loadBaseDataForController];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    [USERDEFAULTS setInteger:2 forKey:@"tabtar"];
    [USERDEFAULTS synchronize];
}

- (void)loadBaseDataForController {
    
    NSDictionary *param = @{
                            @"appId" : APP_ID
                            };

    [[BeeNet sharedInstance] requestWithType:Request_GET url:CREDIT_INDEX param:param success:^(id data) {
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            self.topBankList = [NSArray arrayWithArray:data[@"data"][@"banks"]];
            if (self.topBankList.count > 0) {
                [self setDataTopBankLab];
            }
        } else {
            
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        if(isShowLog == 1) {
            NSLog(@" CREDIT_INDEX-----------%@ ",data);
        }
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" CREDIT_INDEX -------%@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
    
    NSDictionary *params = @{
                             @"appId" : APP_ID,
                             @"pageNum" : [NSString stringWithFormat:@"%ld",self.page],
                             @"pageSize" : @"10"
                             };
    [[BeeNet sharedInstance] requestWithType:Request_GET url:CREDIT_CREDIT_LIST param:params success:^(id data) {
        
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            NSArray *sectionArr = [NSArray arrayWithArray:data[@"data"][@"list"]];
            if (sectionArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [self.tableView.mj_footer endRefreshing];
                [self.cardList addObjectsFromArray:sectionArr];
                [self.tableView reloadData];
                self.page ++;
            }
        } else {
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        if(isShowLog == 1) {
            NSLog(@" CREDIT_CREDIT_LIST------------%@ ",data);
        }
        
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" CREDIT_CREDIT_LIST error ----%@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
    
}

- (void)setDataTopBankLab {
    if (self.topBankList.count != 3) {
        self.threeView.hidden = YES;
        self.fureView.hidden = YES;
//        CGFloat oldHeight = SCREEN_WIDTH * 205 / 375;
//        self.topHeight.constant = oldHeight - (self.bigView.frame.size.height - self.twoView.frame.size.height);
//        CGFloat oldHeight = SCREEN_WIDTH * 166 / 375;
//        CGFloat viewH = self.twoView.frame.size.height;
//        self.contentHeight.constant = 50;
//        self.heghtMagim.constant = 30;
        NSLog(@"=========newheight=========%lf",self.contentHeight.constant);
        [self.twoImgView setImage:[UIImage imageNamed:@"全部功能"]];
        self.twoBankNameLab.text = @"全部银行";
        self.twoSloganLab.text = @"查看更多信用卡";
        
    }
    [self.oneImgView sd_setImageWithURL:[NSURL URLWithString:self.topBankList[0][@"logo"]]];
    self.oneBankNameLan.text = self.topBankList[0][@"name"];
    self.oneBankSloganLab.text = self.topBankList[0][@"slogan"];
    if (self.topBankList.count > 1) {
        [self.twoImgView sd_setImageWithURL:[NSURL URLWithString:self.topBankList[1][@"logo"]]];
        self.twoBankNameLab.text = self.topBankList[1][@"name"];
        self.twoSloganLab.text = self.topBankList[1][@"slogan"];
        
        [self.threeImgView sd_setImageWithURL:[NSURL URLWithString:self.topBankList[2][@"logo"]]];
        self.threeBankLab.text = self.topBankList[2][@"name"];
        self.threeSloganLab.text = self.topBankList[2][@"slogan"];
    }
   
}

- (IBAction)firstBankViewAct:(id)sender {
    if (self.topBankList.count) {
        [self jumpToHtmlViewWithURL:self.topBankList[0][@"jumpUrl"]];
    }
    
}

- (IBAction)secondBankViewAct:(id)sender {
    
    if (self.threeView.hidden) {
        [self performSegueWithIdentifier:@"allBankJump" sender:nil];
    } else {
        if (self.topBankList.count) {
            [self jumpToHtmlViewWithURL:self.topBankList[1][@"jumpUrl"]];
        }
    }
    
}

- (IBAction)thirdBankViewAct:(id)sender {
    if (self.topBankList.count) {
        [self jumpToHtmlViewWithURL:self.topBankList[2][@"jumpUrl"]];
    }
}

- (IBAction)allBankAct:(id)sender {
    [self performSegueWithIdentifier:@"allBankJump" sender:nil];
}

#pragma mark ====================About Table View Code====================
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cardList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HotCardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cardCells" forIndexPath:indexPath];
    if(cell == nil) {
        cell = [[HotCardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cardCells"];
    }
    NSDictionary *dict = self.cardList[indexPath.row];
    [cell.cardImgView sd_setImageWithURL:dict[@"logo"]];
    cell.titleLab.text = dict[@"name"];
    cell.markStr = dict[@"tag"];
    cell.sonNumLab.text = dict[@"num"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return SCREEN_WIDTH * 80 / 375;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == tableView.indexPathsForVisibleRows.lastObject.row) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.topBankList.count != 3) {
                self.heghtMagim.constant = (SCREEN_WIDTH * 166 / 375) - ((SCREEN_WIDTH * 166 / 375) / 2);
                self.contentHeight.constant = self.tableView.contentSize.height + self.twoView.frame.size.height ;
            } else {
                self.contentHeight.constant = self.tableView.contentSize.height + (SCREEN_WIDTH * 80 / 375);
            }
            
        });
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([USERDEFAULTS objectForKey:@"token"]) {
        NSDictionary *dict = self.cardList[indexPath.row];
        [self jumpToHtmlViewWithURL:dict[@"jumpUrl"]];
    } else {
        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
        SignInVC *vc = [board instantiateViewControllerWithIdentifier:@"signInVC"];
        vc.isShowBack = 0;
        vc.backBlock = ^{
            NSDictionary *dict = self.cardList[indexPath.row];
            [self jumpToHtmlViewWithURL:dict[@"jumpUrl"]];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }

}

#pragma mark ====================Jump To Html View====================
- (void)jumpToHtmlViewWithURL:(NSString *)url {
    
    if ([USERDEFAULTS objectForKey:@"token"]) {
        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
        ShowWebViewVC *vc = [board instantiateViewControllerWithIdentifier:@"showWebViewVC"];
        vc.urlStr = url;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
        SignInVC *vc = [board instantiateViewControllerWithIdentifier:@"signInVC"];
        vc.isShowBack = 0;
        vc.backBlock = ^{
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
            ShowWebViewVC *vc = [board instantiateViewControllerWithIdentifier:@"showWebViewVC"];
            vc.urlStr = url;
            [self.navigationController pushViewController:vc animated:YES];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
