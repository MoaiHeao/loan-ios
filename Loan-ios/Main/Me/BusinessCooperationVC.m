//
//  BusinessCooperationVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/31.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "BusinessCooperationVC.h"

@interface BusinessCooperationVC ()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIButton *emaliBtn;

@end

@implementation BusinessCooperationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UISwipeGestureRecognizer *sgr = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(downFun:)];
    sgr.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:sgr];
    [self loadBaseDataForControlller];
}

- (void)loadBaseDataForControlller {
    
    NSDictionary *params = @{
                             @"type" : @"2"
                             };
    [[BeeNet sharedInstance] requestWithType:Request_GET url:CENTER_CONFIG  param:params success:^(id data) {
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            NSDictionary *dict = [NSDictionary dictionaryWithDictionary:data[@"data"]];
            self.titleLab.text = dict[@"title"];
            [self.emaliBtn setTitle:dict[@"content"] forState:UIControlStateNormal];
        } else {
//            [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"%@",data[@"message"]]];
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        if(isShowLog == 1) {
            NSLog(@" -----------%@ ",data);
        }
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" CENTER_CONFIG -------%@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
}

- (void)downFun:(UISwipeGestureRecognizer *)sgr {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)backAct:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
