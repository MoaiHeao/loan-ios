//
//  FeedBackTypeVC.m
//  Loan-ios
//
//  Created by Hao on 2018/11/7.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "FeedBackTypeVC.h"
#import "SelectMessageDetailCell.h"
@interface FeedBackTypeVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contenHeight;

@property (nonatomic, strong) NSArray *feedBackTypeList;
@end
//
@implementation FeedBackTypeVC


- (NSArray *)feedBackTypeList {
    if (!_feedBackTypeList) {
        _feedBackTypeList = @[@"审核进度", @"流程繁琐", @"产品难选", @"使用体验", @"其他"];
    }
    return _feedBackTypeList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self slidingReturnToPreviousPage];
    
}

- (void)slidingReturnToPreviousPage {
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(returnPreviousPage:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipe];
}

- (void)returnPreviousPage:(UISwipeGestureRecognizer *)swipe {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)backAct:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark ====================About Table View Code====================
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.feedBackTypeList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *contentStr = self.feedBackTypeList[indexPath.row];
    SelectMessageDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"feedbackCells" forIndexPath:indexPath];
    if(!cell) {
        cell = [[SelectMessageDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"feedbackCells"];
    }
    cell.selectDetailLab.text = contentStr;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return SCREEN_WIDTH * 40 / 375;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == tableView.indexPathsForVisibleRows.lastObject.row) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.contenHeight.constant = self.tableView.contentSize.height;
        });
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *didseleteStr = self.feedBackTypeList[indexPath.row];
    self.didseleInfoIs(didseleteStr);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
