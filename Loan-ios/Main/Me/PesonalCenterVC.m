//
//  PesonalCenterVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/30.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "PesonalCenterVC.h"
#import "SignInVC.h"
#import "BeeNetCallback.h"
@interface PesonalCenterVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *meIconImgView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLab;
@property (weak, nonatomic) IBOutlet UILabel *accountLab;

@property (strong,nonatomic) UIImagePickerController* imagePicker;
@property (nonatomic, strong) UIImage *seledImg;

@end

@implementation PesonalCenterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]};
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
//    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithHexString:@"#72B4FF"]];
    
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = NO;
    
    
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.meIconImgView.layer.cornerRadius = self.meIconImgView.frame.size.height / 2;
    self.meIconImgView.layer.masksToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:YES];
    
//    [USERDEFAULTS setInteger:0 forKey:@"tabtar"];
//    [USERDEFAULTS synchronize];
    
    if ([USERDEFAULTS objectForKey:@"token"]) {
        
        NSDictionary *params = @{
                                 @"appId" : APP_ID,
                                 @"token" : [USERDEFAULTS objectForKey:@"token"]
                                 };
        [[BeeNet sharedInstance] requestWithType:Request_GET url:LOGIN_TOKEN param:params success:^(id data) {
            NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
            if (returnCode == 200) {
                if ([data[@"data"] intValue] == 0) {
                    [SVProgressHUD showInfoWithStatus:@"您的账号在其他设备登录为确保账户安全请重新登录!"];
                    [USERDEFAULTS removeObjectForKey:@"token"];
                    [USERDEFAULTS synchronize];
                    [self performSegueWithIdentifier:@"toLoginVC" sender:@"0"];
                } else {
                    [self loadBaseDataForController];
                }
            } else {
                
                [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
            }
            if(isShowLog == 1) {
                NSLog(@" -----------%@ ",data);
            }
        } fail:^(NSString *message) {
            if(isShowLog == 1) {
                NSLog(@" LOGIN_TOKEN -------%@ ",message);
            }
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }];
        
        
    } else {
        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
        SignInVC *vc = [board instantiateViewControllerWithIdentifier:@"signInVC"];
        vc.isShowBack = 1;
        vc.fanhuiBlock = ^{
            self.tabBarController.selectedIndex = [[USERDEFAULTS valueForKey:@"tabtar"] intValue];
        };
        vc.backBlock = ^{
            [self loadBaseDataForController];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    

}


- (void)loadBaseDataForController {
    
    [[BeeNet sharedInstance] requestWithType:Request_GET url:CENTER_INDEX param:nil success:^(id data) {
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            NSDictionary *dict = [NSDictionary dictionaryWithDictionary:data[@"data"]];
            self.userNameLab.text = dict[@"name"];
            self.accountLab.text = dict[@"phone"];
            if (dict[@"avatar"]) {
                [self.meIconImgView sd_setImageWithURL:dict[@"avatar"]];
            }
            
        } else {
            
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        if(isShowLog == 1) {
            NSLog(@" -----------%@ ",data);
        }
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" center index error -------%@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
    
}
#pragma mark - 修改头像
- (IBAction)changeIconImgAct:(id)sender {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"提示" message:nil preferredStyle:([self getDeviceIsIpad] ? UIAlertControllerStyleAlert :UIAlertControllerStyleActionSheet)];
    UIAlertAction* fromCamer = [UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.imagePicker animated:YES completion:nil];
    }];
    UIAlertAction* fromLib = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.imagePicker animated:YES completion:nil];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDestructive handler:nil];
    
    [alert addAction:fromCamer];
    [alert addAction:fromLib];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [self.meIconImgView setImage:info[UIImagePickerControllerOriginalImage]];
    self.seledImg = info[UIImagePickerControllerOriginalImage];
    [self upLoadIcon];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)getDeviceIsIpad {
    UIDevice *currentDevice = [UIDevice currentDevice];
    return currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad;
}

//上传二进制文件
- (void)upLoadIcon {
    
    [SVProgressHUD showWithStatus:@"头像上传中..."];
    NSData *imageData = UIImageJPEGRepresentation(self.seledImg, 0.1);//image为要上传的图片(UIImage)
    BeeNetCallback *back = [BeeNetCallback networkWithOriginSuccess:^(id data) {
        if(isShowLog == 1) {
            NSLog(@" =======?%@ ",data);
        }
    } andFailed:^(id data) {
        if(isShowLog == 1) {
            NSLog(@" --------upload img error----%@ ",data);
        }
    }];
    [[BeeNet sharedInstance] postFileWithUrl:COMMON_FILE andFileKey:@"file" andParams:nil andFileData:imageData andHeader:nil andBeeCallback:back andBlock:^(id data) {
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            NSString *imgNetPath = [NSString stringWithFormat:@"%@",data[@"data"]];
            [self uploadImgPathWithStr:imgNetPath];
        } else {
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        if(isShowLog == 1) {
            NSLog(@" =======?%@ ",data);
        }
    }];
    
}
//获取图片的服务器路径后上传路径
- (void)uploadImgPathWithStr:(NSString *)path {
 
    NSDictionary *params = @{
                             @"avatar" : path
                             };
    [[BeeNet sharedInstance] requestWithType:Request_POST url:CENTER_UPDATE_AVATAR param:params success:^(id data) {
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            [SVProgressHUD showSuccessWithStatus:@"上传成功!"];
        } else {
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        if(isShowLog == 1) {
            NSLog(@" -----------%@ ",data);
        }
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" CENTER_UPDATE_AVATAR -------%@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
}

#pragma mark - 我的资料
- (IBAction)myInfoAct:(id)sender {
    [self performSegueWithIdentifier:@"meToMyInfoVC" sender:nil];
}

#pragma mark - 帮助中心
- (IBAction)hepleCenterAct:(id)sender {
    [self performSegueWithIdentifier:@"meToHelpCenterVC" sender:nil];
}

#pragma mark - 关于我们
- (IBAction)aboutUsAct:(id)sender {
    [self performSegueWithIdentifier:@"meToAboutUsVC" sender:nil];
}

#pragma mark - 商务合作
- (IBAction)businessComAct:(id)sender {
    [self performSegueWithIdentifier:@"meToBusinessCooperationVC" sender:nil];
}

#pragma mark - 分享好友
- (IBAction)shareToFrientsAct:(id)sender {
//    [self performSegueWithIdentifier:@"meToShareVC" sender:nil];
    [self performSegueWithIdentifier:@"meToFeedBackVC" sender:nil];
}

#pragma mark - 意见反馈
- (IBAction)feedBackAct:(id)sender {
    [self performSegueWithIdentifier:@"meToFeedBackVC" sender:nil];
}

#pragma mark - 退出登录
- (IBAction)signOutBtnAct:(id)sender {
    [SVProgressHUD showWithStatus:@"正在退出..."];
    [[BeeNet sharedInstance] requestWithType:Request_POST url:CENTER_LOGOUT param:nil success:^(id data) {
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            [SVProgressHUD dismiss];
            [USERDEFAULTS removeObjectForKey:@"token"];
            [USERDEFAULTS synchronize];
            
            [self performSegueWithIdentifier:@"toLoginVC" sender:@"1"];
        } else {
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"服务器正忙!%@",data[@"message"]]];
        }
        if(isShowLog == 1) {
            NSLog(@" -----------%@ ",data);
        }
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" CENTER_LOGOUT -------%@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
    

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(NSString *)sender {
    if ([segue.identifier isEqualToString:@"toLoginVC"]) {
        SignInVC *vc = segue.destinationViewController;
        vc.backBlock = ^{
            self.tabBarController.selectedIndex = 0;
        };
        vc.fanhuiBlock = ^{
             self.tabBarController.selectedIndex = 0;
        };
        vc.isShowBack = sender.integerValue;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

*/

@end
