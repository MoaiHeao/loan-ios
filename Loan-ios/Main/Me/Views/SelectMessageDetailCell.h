//
//  SelectMessageDetailCell.h
//  Loan-ios
//
//  Created by Hao on 2018/11/2.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectMessageDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *selectDetailLab;

@end
