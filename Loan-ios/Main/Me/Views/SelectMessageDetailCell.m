//
//  SelectMessageDetailCell.m
//  Loan-ios
//
//  Created by Hao on 2018/11/2.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "SelectMessageDetailCell.h"

@implementation SelectMessageDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
