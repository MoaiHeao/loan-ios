//
//  HelpCenterCell.h
//  Loan-ios
//
//  Created by Hao on 2018/11/1.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpCenterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIImageView *upDownImgView;
@property (weak, nonatomic) IBOutlet UIView *showContentView;
@property (weak, nonatomic) IBOutlet UITextView *contentLAb;

@property (nonatomic, assign) BOOL isShowContent;
@property (nonatomic, assign) BOOL open;

@end
