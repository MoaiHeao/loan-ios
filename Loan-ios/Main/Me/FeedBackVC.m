//
//  FeedBackVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/31.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "FeedBackVC.h"
#import "FeedBackTypeVC.h"
@interface FeedBackVC ()
@property (weak, nonatomic) IBOutlet UILabel *typeLab;
@property (weak, nonatomic) IBOutlet UITextView *feedBactContentTF;

@end

@implementation FeedBackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationController.navigationBar.tintColor  = [UIColor whiteColor];
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]};
}

- (IBAction)selectTypeAct:(id)sender {
    [self performSegueWithIdentifier:@"selectFeedBackTypeVC" sender:@"selectType"];
}


- (IBAction)submitBtnAct:(id)sender {
    
    if (self.feedBactContentTF.text.length == 0 || [self.typeLab.text isEqualToString:@"请选择"]) {
        [SVProgressHUD showErrorWithStatus:@"请选择反馈类型或填写反馈信息!"];
    } else {
        
        NSDictionary *params = @{
                                 @"type" : self.typeLab.text,
                                 @"content" : self.feedBactContentTF.text
                                 };
        [SVProgressHUD showWithStatus:@"提交中..."];
        [[BeeNet sharedInstance] requestWithType:Request_POST url:CENTER_FEEDBACK param:params success:^(id data) {
            NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
            if (returnCode == 200) {
                [SVProgressHUD showSuccessWithStatus:@"反馈成功!"];
                [SVProgressHUD dismissWithCompletion:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                
            } else {
                [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
            }
            if(isShowLog == 1) {
                NSLog(@" ---------%@ ",data);
            }
        } fail:^(NSString *message) {
            if(isShowLog == 1) {
                NSLog(@" feedback error ---------%@ ",message);
            }
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }];
        
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([sender isEqualToString:@"selectType"]) {
        FeedBackTypeVC *vc = segue.destinationViewController;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.typeLab.text = msg;
        };
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

*/

@end
