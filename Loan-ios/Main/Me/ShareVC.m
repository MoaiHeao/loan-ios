//
//  ShareVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/31.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "ShareVC.h"
#import "WXApi.h"
@interface ShareVC ()

@end

@implementation ShareVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UISwipeGestureRecognizer *sgr = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(downFun:)];
    sgr.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:sgr];
    
}
- (IBAction)backAct:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)downFun:(UISwipeGestureRecognizer *)sgr {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//分享给朋友
- (IBAction)wxShareToSessionACT:(id)sender {
//    [self wecahtShareTo:1];
}

//分享到朋友圈
- (IBAction)wxShareToTimeLineAct:(id)sender {
//    [self wecahtShareTo:2];
}

- (void)wecahtShareTo:(NSInteger)type {
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = @"标题";
    message.description = @"详情";
    [message setThumbImage:[UIImage imageNamed:@"logo"]];
    
    WXWebpageObject *ext = [WXWebpageObject object];
    ext.webpageUrl = @"";
    
    message.mediaObject = ext;
    
    req.message = message;
    if (type == 1) {
        req.scene = WXSceneSession;
    } else {
        req.scene = WXSceneTimeline;
    }
    
    [WXApi sendReq:req];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
