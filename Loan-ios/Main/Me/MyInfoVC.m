//
//  MyInfoVC.m
//  Loan-ios
//
//  Created by Hao on 2018/11/1.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "MyInfoVC.h"
#import "JFCityViewController.h"
#import "SelectMessageVC.h"



@interface MyInfoVC ()<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *buleLineView;
@property (weak, nonatomic) IBOutlet UIScrollView *infoScrollView;
@property (weak, nonatomic) IBOutlet UIView *topView;

//个人信息
@property (weak, nonatomic) IBOutlet UITextField *userNameTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *iDCardTF;
@property (weak, nonatomic) IBOutlet UILabel *locatCityLab;
@property (weak, nonatomic) IBOutlet UILabel *maritalStstusLab;
@property (weak, nonatomic) IBOutlet UILabel *educationalLevelLab;
@property (weak, nonatomic) IBOutlet UITextField *sesameBranchTF;

//身份信息
@property (weak, nonatomic) IBOutlet UILabel *professionalIdentityLab;
@property (weak, nonatomic) IBOutlet UILabel *formOfIncomeLab;
@property (weak, nonatomic) IBOutlet UITextField *monthlyIncomeTF;
@property (weak, nonatomic) IBOutlet UILabel *typeOfCompanyLab;
@property (weak, nonatomic) IBOutlet UILabel *workingTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *payProvidentFundLab;
@property (weak, nonatomic) IBOutlet UILabel *paySocialSecurityLab;

//资产信息
@property (weak, nonatomic) IBOutlet UILabel *hasCreditCardLab;
@property (weak, nonatomic) IBOutlet UILabel *housePropertyLab;
@property (weak, nonatomic) IBOutlet UILabel *carPropertyLab;
@property (weak, nonatomic) IBOutlet UILabel *loanSeccessLab;
@property (weak, nonatomic) IBOutlet UILabel *creditSituationLab;

@property (weak, nonatomic) IBOutlet UIButton *saveInfoBtn;

@property (nonatomic, assign) NSInteger upLoadAll;
@end

@implementation MyInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationController.navigationBar.tintColor  = [UIColor whiteColor];
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]};
    self.infoScrollView.delegate = self;
    self.upLoadAll = 0;
    self.topView.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self loadBaseDataForController];
}

- (void)keyboardWillShow:(NSNotification *)noti {
    
    self.infoScrollView.scrollEnabled = NO;
    self.topView.hidden = NO;
}

- (void)keyboardWillHide:(NSNotification *)noti {
    
    self.infoScrollView.scrollEnabled = YES;
    self.topView.hidden = YES;
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGPoint center = self.buleLineView.center;
    center.x = SCREEN_WIDTH / 6;
    self.buleLineView.center = center;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(scrollView == self.infoScrollView) {
        CGFloat showViewX = self.infoScrollView.contentOffset.x + 100;
        int viewNum = showViewX / SCREEN_WIDTH;
        int count = 1;
        if(viewNum == 0) {
            count = 1;
            
        } else if (viewNum == 1) {
            count = 3;
            
        } else {
            count = 5;
            
        }
        
        CGPoint center = self.buleLineView.center;
        center.x = SCREEN_WIDTH / 6 * count;
        [UIView animateWithDuration:0.1 animations:^{
            self.buleLineView.center = center;
        }];
    }
}

- (void)loadBaseDataForController {
    
    [[BeeNet sharedInstance] requestWithType:Request_GET url:CENTER_INFO param:nil success:^(id data) {
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            NSDictionary *dict = [NSDictionary dictionaryWithDictionary:data[@"data"]];
            [self setBaseDataForControllerWithData:dict];
        } else {
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        if(isShowLog == 1) {
            NSLog(@" -----------%@ ",data);
        }
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" CENTER_INFO -------%@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
    
}

- (void)setBaseDataForControllerWithData:(NSDictionary *)dict {

    //    //个人信息
//    [NSString stringWithFormat:dict[@"name"]]
    self.userNameTF.text = [self judgeContentWithStr:dict[@"name"]] ? dict[@"name"] : nil;
    self.phoneNumberTF.text = [self judgeContentWithStr:dict[@"phone"]] ? dict[@"phone"] : nil;
    self.iDCardTF.text = [self judgeContentWithStr:dict[@"idCard"]] ? dict[@"idCard"] : nil;
    self.locatCityLab.text = [self judgeContentWithStr:dict[@"city"]] ? dict[@"city"] : @"请选择";
    self.maritalStstusLab.text = [self judgeContentWithStr:dict[@"marriage"]] ? dict[@"name"] : @"请选择";
    self.educationalLevelLab.text = [self judgeContentWithStr:dict[@"culture"]] ? dict[@"culture"] : @"请选择";
    self.sesameBranchTF.text = [self judgeContentWithStr:[NSString stringWithFormat:@"%@",dict[@"sesame"]]] ? [NSString stringWithFormat:@"%@",dict[@"sesame"]] : nil;
//
//    //    //身份信息
    NSDictionary *idenityDict = [NSDictionary dictionaryWithDictionary:dict[@"userIdentity"]];
    self.professionalIdentityLab.text = [self judgeContentWithStr:idenityDict[@"identity"]] ? idenityDict[@"identity"] : @"请选择";
    self.formOfIncomeLab.text = [self judgeContentWithStr:idenityDict[@"incomeForm"]] ? idenityDict[@"incomeForm"] : @"请选择";
    self.monthlyIncomeTF.text = [self judgeContentWithStr:idenityDict[@"income"]] ? idenityDict[@"income"] : nil;
    self.typeOfCompanyLab.text = [self judgeContentWithStr:idenityDict[@"type"]] ? idenityDict[@"type"] : @"请选择";
    self.workingTimeLab.text = [self judgeContentWithStr:idenityDict[@"workTime"]] ? idenityDict[@"workTime"] : @"请选择";
    self.payProvidentFundLab.text = [self judgeContentWithStr:idenityDict[@"fund"]] ? idenityDict[@"fund"] : @"请选择";
    self.paySocialSecurityLab.text = [self judgeContentWithStr:idenityDict[@"insurance"]] ? idenityDict[@"insurance"] : @"请选择";
//
//    //    //资产信息
    NSDictionary *assetDict = [NSDictionary dictionaryWithDictionary:dict[@"userAsset"]];
    self.hasCreditCardLab.text = [self judgeContentWithStr:assetDict[@"card"]] ? assetDict[@"card"] : @"请选择";
    self.housePropertyLab.text = [self judgeContentWithStr:assetDict[@"house"]] ? assetDict[@"house"] : @"请选择";
    self.carPropertyLab.text = [self judgeContentWithStr:assetDict[@"car"]] ? assetDict[@"car"] : @"请选择";
    self.loanSeccessLab.text = [self judgeContentWithStr:assetDict[@"loan"]] ? assetDict[@"loan"] : @"请选择";
    self.creditSituationLab.text = [self judgeContentWithStr:assetDict[@"credit"]] ? assetDict[@"credit"] : @"请选择";
}

//赋值时判断请求到有没有数据
- (BOOL)judgeContentWithStr:(NSString *)str {
    if (str.length == 0 || [str isEqualToString:@"请选择"] || [str isEqualToString:@"(null)"]) {
        return NO;
    } else {
        return YES;
    }
}

//上传参数是判读是否用户有填写信息
- (NSString *)judgeContenWithParams:(NSString *)str {
    
    if (str.length == 0 || [str isEqualToString:@"请选择"] || [str isEqualToString:@"(null)"]) {
        return @"";
    } else {
        return str;
    }
    
}

#pragma mark - 个人信息
- (IBAction)pensonalInfoAct:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
        self.infoScrollView.contentOffset = CGPointMake(0 , 0);
    }];
}

//选择城市
- (IBAction)selectCityAct:(id)sender {
    JFCityViewController *vc = [[JFCityViewController alloc] init];
    vc.title = @"选择城市";
    [vc choseCityBlock:^(NSString *cityName) {
        self.locatCityLab.text = cityName;
    }];
    
    [self.navigationController pushViewController:vc animated:YES];
}

//婚姻状况
- (IBAction)maritalStstusAct:(id)sender {
    [self performSegueWithIdentifier:@"showSelectMessageVC" sender:@"maritalStstus"];
}

//文化程度
- (IBAction)educationalLevelAct:(id)sender {
    [self performSegueWithIdentifier:@"showSelectMessageVC" sender:@"educationalLevel"];
}

#pragma mark - 身份信息
- (IBAction)identityInfoAct:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
        self.infoScrollView.contentOffset = CGPointMake(SCREEN_WIDTH, 0);
    }];
}
//职业身份
- (IBAction)professionalIdentityAct:(id)sender {
    [self performSegueWithIdentifier:@"showSelectMessageVC" sender:@"professionalIdentity"];
}

//收入形式
- (IBAction)formOfIncomeAct:(id)sender {
    [self performSegueWithIdentifier:@"showSelectMessageVC" sender:@"formOfIncome"];
}

//公司类型
- (IBAction)typeOfCompanyAct:(id)sender {
    [self performSegueWithIdentifier:@"showSelectMessageVC" sender:@"typeOfCompany"];
}

//工作时间
- (IBAction)workingTimeAct:(id)sender {
    [self performSegueWithIdentifier:@"showSelectMessageVC" sender:@"workingTime"];
}

//缴纳公积金
- (IBAction)payProvidentFundAct:(id)sender {
    [self performSegueWithIdentifier:@"showSelectMessageVC" sender:@"payProvidentFund"];
}

//缴纳社保
- (IBAction)paySocialSecurityAct:(id)sender {
    [self performSegueWithIdentifier:@"showSelectMessageVC" sender:@"paySocialSecurity"];
}

#pragma mark - 资产信息
- (IBAction)assetsInfoAct:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
        self.infoScrollView.contentOffset = CGPointMake(SCREEN_WIDTH * 2, 0);
    }];
}
//是否有信用卡
- (IBAction)hasCreditCardAct:(id)sender {
    [self performSegueWithIdentifier:@"showSelectMessageVC" sender:@"hasCreditCard"];
}

//房产
- (IBAction)housePropertyAct:(id)sender {
    [self performSegueWithIdentifier:@"showSelectMessageVC" sender:@"houseProperty"];
}

//车产
- (IBAction)carPropertyAct:(id)sender {
    [self performSegueWithIdentifier:@"showSelectMessageVC" sender:@"carProperty"];
}

//贷款成功记录
- (IBAction)loanSeccessAct:(id)sender {
    [self performSegueWithIdentifier:@"showSelectMessageVC" sender:@"loanSeccess"];
}

//信用情况
- (IBAction)creditSituationAct:(id)sender {
    [self performSegueWithIdentifier:@"showSelectMessageVC" sender:@"creditSituation"];
}

#pragma mark - 保存
- (IBAction)saveInfoBtnAct:(id)sender {
    
    [self postUserInfoMessageWithShowMessage:1];

}


- (void)postUserInfoMessageWithShowMessage:(NSInteger)show {
//    if (show == 1) {
//        [SVProgressHUD showWithStatus:@"信息保存中..."];
//    }
    //个人信息
    NSDictionary *paramsA = @{
                              @"name" : [self judgeContenWithParams:self.userNameTF.text],
                              @"phone" : [self judgeContenWithParams:self.phoneNumberTF.text],
                              @"idCard" : [self judgeContenWithParams:self.iDCardTF.text],
                              @"city" : [self judgeContenWithParams:self.locatCityLab.text],
                              @"marriage" : [self judgeContenWithParams:self.maritalStstusLab.text],
                              @"culture" : [self judgeContenWithParams:self.educationalLevelLab.text],
                              @"sesame" : [self judgeSesame:[self judgeContenWithParams:self.sesameBranchTF.text]]
                              };
    [self uploadDateToNetWithURL:CENTER_UPDATE_INFO andParams:paramsA andLast:0 andShowMessage:show];
    //身份信息
    NSDictionary *paramsB = @{
                              @"identity" : [self judgeContenWithParams:self.professionalIdentityLab.text],
                              @"incomeForm" : [self judgeContenWithParams:self.formOfIncomeLab.text],
                              @"income" : [self judgeContenWithParams:self.monthlyIncomeTF.text],
                              @"type" : [self judgeContenWithParams:self.typeOfCompanyLab.text],
                              @"workTime" : [self judgeContenWithParams:self.workingTimeLab.text],
                              @"fund" : [self judgeContenWithParams:self.payProvidentFundLab.text],
                              @"insurance" : [self judgeContenWithParams:self.paySocialSecurityLab.text]
                              };
    [self uploadDateToNetWithURL:CENTER_UPDATE_IDENTITY andParams:paramsB andLast:0 andShowMessage:show];
    //资产信息
    NSDictionary *paramsC = @{
                              @"card" : [self judgeContenWithParams:self.hasCreditCardLab.text],
                              @"house" : [self judgeContenWithParams:self.housePropertyLab.text],
                              @"car" : [self judgeContenWithParams:self.carPropertyLab.text],
                              @"loan" : [self judgeContenWithParams:self.loanSeccessLab.text],
                              @"credit" : [self judgeContenWithParams:self.creditSituationLab.text]
                              };
    [self uploadDateToNetWithURL:CENTER_UPDATE_ASSET andParams:paramsC andLast:1 andShowMessage:show];
    
}

- (NSString *)judgeSesame:(NSString *)str {
    
    if (str.length == 0) {
        return @"0";
    } else {
        return str;
    }
}

- (void)uploadDateToNetWithURL:(NSString *)postURL andParams:(NSDictionary *)params andLast:(NSInteger)last andShowMessage:(NSInteger)show {
    
    [[BeeNet sharedInstance] requestWithType:Request_POST url:postURL param:params success:^(id data) {
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            self.upLoadAll ++;
            if (show == 1) {
                if (last == 1) {
                    if (self.upLoadAll == 3) {
                        [SVProgressHUD showSuccessWithStatus:@"信息保存成功!"];
                    } else {
                        //                    [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
                    }
                }
            }
        } else {
            
        }
        
        if(isShowLog == 1) {
            NSLog(@" -----------%@ ",data);
        }
        
        
        
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" %@ -------%@ ",postURL,message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    SelectMessageVC *vc = segue.destinationViewController;
    if([sender isEqualToString:@"maritalStstus"]) {
        //婚姻状况
        vc.selectType = 4;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.maritalStstusLab.text = msg;
        };
    } else if ([sender isEqualToString:@"educationalLevel"]) {
        //文化程度
        vc.selectType = 2;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.educationalLevelLab.text = msg;
        };
    } else if ([sender isEqualToString:@"professionalIdentity"]) {
        //职业身份
        vc.selectType = 3;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.professionalIdentityLab.text = msg;
        };
    } else if ([sender isEqualToString:@"formOfIncome"]) {
        //收入形式
        vc.selectType = 5;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.formOfIncomeLab.text = msg;
        };
    } else if ([sender isEqualToString:@"typeOfCompany"]) {
        //公司类型
        vc.selectType = 6;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.typeOfCompanyLab.text = msg;
        };
    } else if ([sender isEqualToString:@"workingTime"]) {
        //工作时间
        vc.selectType = 7;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.workingTimeLab.text = msg;
        };
    } else if ([sender isEqualToString:@"payProvidentFund"]) {
        //缴纳公积金
        vc.selectType = 1;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.payProvidentFundLab.text = msg;
        };
    } else if ([sender isEqualToString:@"paySocialSecurity"]) {
        //缴纳社保
        vc.selectType = 1;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.paySocialSecurityLab.text = msg;
        };
    } else if ([sender isEqualToString:@"hasCreditCard"]) {
        //是否有信用卡
        vc.selectType = 1;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.hasCreditCardLab.text = msg;
        };
    } else if ([sender isEqualToString:@"houseProperty"]) {
        //房产
        vc.selectType = 8;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.housePropertyLab.text = msg;
        };
    } else if ([sender isEqualToString:@"carProperty"]) {
        //车产
        vc.selectType = 9;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.carPropertyLab.text = msg;
        };
    } else if ([sender isEqualToString:@"loanSeccess"]) {
        //贷款成功记录
        vc.selectType = 1;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.loanSeccessLab.text = msg;
        };
    } else if ([sender isEqualToString:@"creditSituation"]) {
        //信用情况
        vc.selectType = 10;
        vc.didseleInfoIs = ^(NSString *msg) {
            self.creditSituationLab.text = msg;
        };
    }
    
}


- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:YES];
    [self postUserInfoMessageWithShowMessage:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
