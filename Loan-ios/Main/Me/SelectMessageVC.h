//
//  SelectMessageVC.h
//  Loan-ios
//
//  Created by Hao on 2018/11/2.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectMessageVC : UIViewController
@property (nonatomic, assign) NSInteger selectType;

@property (nonatomic, copy) void (^didseleInfoIs)(NSString *msg);
@end
