//
//  SignInVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/31.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "SignInVC.h"

@interface SignInVC ()
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *verificationCodeTF;
@property (weak, nonatomic) IBOutlet UIView *getVerificationView;
@property (weak, nonatomic) IBOutlet UILabel *getVerificationLab;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *getVerificationCodeAct;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSInteger countDownNumber;


@end

@implementation SignInVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationController.navigationBar.tintColor  = [UIColor whiteColor];
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]};
    self.countDownNumber = 59;
    
    self.navigationItem.hidesBackButton = self.isShowBack;
    if (self.isShowBack) {
        UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Chevron"] style:UIBarButtonItemStylePlain target:self action:@selector(backActt)];
        self.navigationItem.leftBarButtonItem = back;
    }
    
}

- (void)backActt {
    self.fanhuiBlock();
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.getVerificationView.layer.cornerRadius = self.getVerificationView.frame.size.height / 2;
    self.getVerificationView.layer.masksToBounds = YES;
}

//获取验证码
- (IBAction)getVerificationCodeAct:(id)sender {
    
    if (self.phoneNumberTF.text.length != 11) {
        [SVProgressHUD showErrorWithStatus:@"手机号码不正确"];
    } else {
        
        NSDictionary *param = @{
                                @"phone" : self.phoneNumberTF.text,
                                @"appId" : APP_ID,
                                @"type" : @"0"
                                };
        [SVProgressHUD showWithStatus:@"正在获取验证码..."];
        self.getVerificationCodeAct.enabled = NO;
        [[BeeNet sharedInstance] requestWithType:Request_GET url:LOGIN_CODE param:param success:^(id data) {
            if ([data[@"code"] intValue] == 200) {
                [self countDown];
                if(isShowLog == 1) {
                    NSLog(@"-------%@",data);
                }
                [SVProgressHUD showInfoWithStatus:data[@"data"]];
            } else {
                self.getVerificationCodeAct.enabled = YES;
                [SVProgressHUD showInfoWithStatus:data[@"message"]];
            }
            
        } fail:^(NSString *message) {
            if(isShowLog == 1) {
                NSLog(@"login code error %@",message);
            }
            [SVProgressHUD showErrorWithStatus:@"网络连接失败"];
        }];

        
    }
    
    
}



- (void)countDown {
    self.timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(countdownToDo) userInfo:nil repeats:YES];
    NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
    [runLoop addTimer:self.timer forMode:NSDefaultRunLoopMode];
}

- (void)countdownToDo {
    if(self.countDownNumber == 0) {
        self.getVerificationLab.text = @"获取验证码";
        [self.timer invalidate];
        self.getVerificationCodeAct.enabled = YES;
        self.countDownNumber = 59;
    } else {
        self.getVerificationLab.text = [NSString stringWithFormat:@"%lds",self.countDownNumber];
        self.countDownNumber --;
    }
    if (self.countDownNumber == 45) {
        [SVProgressHUD dismiss];
    }
    
}


- (IBAction)signInAct:(id)sender {
    if(self.phoneNumberTF.text.length != 11 && self.verificationCodeTF.text.length != 4) {
        [SVProgressHUD showErrorWithStatus:@"手机号码或验证码错误!"];
    } else {
        
        NSDictionary *param = @{
                                @"phone" : self.phoneNumberTF.text,
                                @"code" : self.verificationCodeTF.text,
                                @"appId" : APP_ID
                                };
        [SVProgressHUD showWithStatus:@"登录中..."];
        [[BeeNet sharedInstance] requestWithType:Request_POST url:LOGIN_LOGIN param:param success:^(id data) {
            
            if(isShowLog == 1) {
                NSLog(@"-----%@",data);
            }
            
            NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
            if (returnCode == 200) {
                NSString *token = data[@"data"];
                //保存token
                
                [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self.navigationController popViewControllerAnimated:YES];
                self.backBlock();
                [SVProgressHUD dismiss];
            } else {
                [SVProgressHUD showInfoWithStatus:[NSString stringWithFormat:@"%@",data[@"message"]]];
//                [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
            }
            
            
            
        } fail:^(NSString *message) {
            if(isShowLog == 1) {
                NSLog(@"login login error %@",message);
            }
            [SVProgressHUD showErrorWithStatus:@"服务器正忙"];
        }];
    }
}



- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self.timer invalidate];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
