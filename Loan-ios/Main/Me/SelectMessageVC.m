//
//  SelectMessageVC.m
//  Loan-ios
//
//  Created by Hao on 2018/11/2.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "SelectMessageVC.h"
#import "SelectMessageDetailCell.h"
@interface SelectMessageVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@property (nonatomic, strong) NSArray *typeList;

//是否
@property (nonatomic, strong) NSArray *yesOrNoList;
//教育程度
@property (nonatomic, strong) NSArray *educationalLevelList;
//身份职业
@property (nonatomic, strong) NSArray *professionalIdentityList;
//婚姻状况
@property (nonatomic, strong) NSArray *maritalStstusList;
//收入形式
@property (nonatomic, strong) NSArray *formOfIncomeList;
//公司类型
@property (nonatomic, strong) NSArray *typeOfCompanyList;
//工作时间
@property (nonatomic, strong) NSArray *workingTimeList;
//房产
@property (nonatomic, strong) NSArray *housePropertyList;
//车产
@property (nonatomic, strong) NSArray *carPropertyList;
//信用情况
@property (nonatomic, strong) NSArray *creditSituationList;

@end



//@property (weak, nonatomic) IBOutlet UITextField *userNameTF;
//@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTF;
//@property (weak, nonatomic) IBOutlet UITextField *iDCardTF;
//@property (weak, nonatomic) IBOutlet UILabel *locatCityLab;
//@property (weak, nonatomic) IBOutlet UILabel *maritalStstusLab;
//@property (weak, nonatomic) IBOutlet UILabel *educationalLevelLab;
//@property (weak, nonatomic) IBOutlet UITextField *sesameBranchTF;
//
////身份信息
//@property (weak, nonatomic) IBOutlet UILabel *professionalIdentityLab;
//@property (weak, nonatomic) IBOutlet UILabel *formOfIncomeLab;
//@property (weak, nonatomic) IBOutlet UITextField *monthlyIncomeTF;
//@property (weak, nonatomic) IBOutlet UILabel *typeOfCompanyLab;
//@property (weak, nonatomic) IBOutlet UILabel *workingTimeLab;
//@property (weak, nonatomic) IBOutlet UILabel *payProvidentFundLab;
//@property (weak, nonatomic) IBOutlet UILabel *paySocialSecurityLab;
//
////资产信息
//@property (weak, nonatomic) IBOutlet UILabel *hasCreditCardLab;
//@property (weak, nonatomic) IBOutlet UILabel *housePropertyLab;
//@property (weak, nonatomic) IBOutlet UILabel *carPropertyLab;
//@property (weak, nonatomic) IBOutlet UILabel *loanSeccessLab;
//@property (weak, nonatomic) IBOutlet UILabel *creditSituationLab;



@implementation SelectMessageVC
//是否
- (NSArray *)yesOrNoList {
    if(!_yesOrNoList) {
        _yesOrNoList = @[@"是", @"否"];
    }
    return _yesOrNoList;
}
//文化程度
- (NSArray *)educationalLevelList {
    if(!_educationalLevelList) {
        _educationalLevelList = @[@"高中", @"大专", @"本科", @"硕士", @"博士"];
    }
    return _educationalLevelList;
}
//职业身份
- (NSArray *)professionalIdentityList {
    if(!_professionalIdentityList) {
        _professionalIdentityList = @[@"上班族", @"个体户", @"企业主", @"自由职业"];
    }
    return _professionalIdentityList;
}

//婚姻
- (NSArray *)maritalStstusList {
    if (!_maritalStstusList) {
        _maritalStstusList = @[@"未婚", @"已婚"];
    }
    return _maritalStstusList;
}

//收入形式
- (NSArray *)formOfIncomeList {
    if (!_formOfIncomeList) {
        _formOfIncomeList = @[@"银行代发", @"转账工资", @"现金发放"];
    }
    return _formOfIncomeList;
}

//公司类型
- (NSArray *)typeOfCompanyList {
    if (!_typeOfCompanyList) {
        _typeOfCompanyList = @[@"普通企业", @"事业单位", @"大型垄断国企", @"世界500强企业", @"上市企业", @"其他"];
    }
    return _typeOfCompanyList;
}

//工作时间
- (NSArray *)workingTimeList {
    if (!_workingTimeList) {
        _workingTimeList = @[@"不足3个月", @"3-5个月", @"6-12个月", @"1-3年", @"4-7年", @"7年以上"];
    }
    return _workingTimeList;
}

//房产
- (NSArray *)housePropertyList {
    if (!_housePropertyList) {
        _housePropertyList = @[@"无房产", @"商品住房", @"商铺", @"办公室", @"厂房", @"宅基地/自建房"];
    }
    return _housePropertyList;
}
//车产
- (NSArray *)carPropertyList {
    if (!_carPropertyList) {
        _carPropertyList = @[@"无车产", @"名下有车", @"有车但车已抵押"];
    }
    return _carPropertyList;
}
//信用情况
- (NSArray *)creditSituationList {
    if (!_creditSituationList) {
        _creditSituationList = @[@"无信用记录", @"信用记录良好", @"少量逾期", @"征信较差"];
    }
    return _creditSituationList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self slidingReturnToPreviousPage];
    
    switch (self.selectType) {
        case 1 : {
            //是否
            self.typeList = [NSArray arrayWithArray:self.yesOrNoList];
            break;
        } case 2 : {
            //文化程度
            self.typeList = [NSArray arrayWithArray:self.educationalLevelList];
            break;
        } case 3 : {
            //身份职业
            self.typeList = [NSArray arrayWithArray:self.professionalIdentityList];
            break;
        } case 4 : {
            //婚姻状况
            self.typeList = [NSArray arrayWithArray:self.maritalStstusList];
            break;
        } case 5 : {
            //收入形式
            self.typeList = [NSArray arrayWithArray:self.formOfIncomeList];
            break;
        } case 6 : {
            //公司类型
            self.typeList = [NSArray arrayWithArray:self.typeOfCompanyList];
            break;
        } case 7 : {
            //工作时间
            self.typeList = [NSArray arrayWithArray:self.workingTimeList];
            break;
        } case 8 : {
            //房产
            self.typeList = [NSArray arrayWithArray:self.housePropertyList];
            break;
        } case 9 : {
            //车产
            self.typeList = [NSArray arrayWithArray:self.carPropertyList];
            break;
        } case 10 : {
            //信用情况
            self.typeList = [NSArray arrayWithArray:self.creditSituationList];
            break;
        }
        default:
            break;
    }
    
}
- (IBAction)backAct:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)slidingReturnToPreviousPage {
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(returnPreviousPage:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipe];
}

- (void)returnPreviousPage:(UISwipeGestureRecognizer *)swipe {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark ====================About Table View Code====================
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.typeList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *contentStr = self.typeList[indexPath.row];
    SelectMessageDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"selectMessageDetailCell" forIndexPath:indexPath];
    if(!cell) {
        cell = [[SelectMessageDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"selectMessageDetailCell"];
    }
    cell.selectDetailLab.text = contentStr;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return SCREEN_WIDTH * 40 / 375;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == tableView.indexPathsForVisibleRows.lastObject.row) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.contentViewHeight.constant = self.tableView.contentSize.height;
        });
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *didseleteStr = self.typeList[indexPath.row];
    self.didseleInfoIs(didseleteStr);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
