//
//  SignInVC.h
//  Loan-ios
//
//  Created by Hao on 2018/10/31.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInVC : UIViewController
@property (nonatomic, assign) NSInteger isShowBack;
@property (nonatomic, copy) void(^backBlock)(void);

@property (nonatomic, copy) void(^fanhuiBlock)(void);
@end
