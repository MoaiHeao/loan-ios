//
//  HelpCenterVC.m
//  Loan-ios
//
//  Created by Hao on 2018/10/31.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "HelpCenterVC.h"
#import "HelpCenterCell.h"
@interface HelpCenterVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSIndexPath *oldIndexpath;

@property (nonatomic, strong) NSArray *quentionList;

@end

@implementation HelpCenterVC



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationController.navigationBar.tintColor  = [UIColor whiteColor];
//    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]};
    [self loadTableViewLayout];
    [self loadBaseDataForController];
    
}

- (void)loadBaseDataForController {
    
    [[BeeNet sharedInstance] requestWithType:Request_GET url:CENTER_HELP param:nil success:^(id data) {
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            self.quentionList = [NSArray arrayWithArray:data[@"data"]];
            [self.tableView reloadData];
        } else {
            
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        if(isShowLog == 1) {
            NSLog(@" -----------%@ ",data);
        }
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" CENTER_HELP error -------%@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
    
}

#pragma mark ====================About Table View Code====================
//
- (void)loadTableViewLayout {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.quentionList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HelpCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"helpCenterCells" forIndexPath:indexPath];
    if(cell == nil) {
        cell = [[HelpCenterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"helpCenterCells"];
    }
    cell.isShowContent = NO;
    cell.open = YES;
    NSDictionary *dict = self.quentionList[indexPath.row];
    cell.titleLab.text = dict[@"title"];
    cell.contentLAb.text = dict[@"content"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = SCREEN_WIDTH * 60.0 / 375;
    
    if(self.oldIndexpath != nil && indexPath == self.oldIndexpath ) {
        NSDictionary *dict = self.quentionList[indexPath.row];
        NSString *ass = dict[@"content"];
        CGFloat width = SCREEN_WIDTH - 34.0;
//        CGSize size = [ass sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13]}];
        CGRect rect = [ass boundingRectWithSize:CGSizeMake(width, 0) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:20]} context:nil];
        
        NSLog(@"height==================>%lf",rect.size.height);
        return cellHeight + rect.size.height + 5;
    } else {
        
        return cellHeight;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //1、获取当前cell的是否显示详情属性值
    
//    BOOL show = cell.isShowContent;
//    //2、判断值  如果不显示--更新为显示--改变图标
//    //          如果显示--更新为不显示--改变图标
//    cell.isShowContent = !show;
//
//    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HelpCenterCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//    cell.isShowContent = !cell.isShowContent;
//    [self.tableView beginUpdates];
//    [self.tableView endUpdates];
    
    if(indexPath == self.oldIndexpath ) {
        self.oldIndexpath = nil;
//        cell.isShowContent = NO;
        
    } else {
//        cell.isShowContent = YES;
        self.oldIndexpath = indexPath;
        
    }
    
    cell.open = !cell.open;
//    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView reloadData];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
