//
//  Constant.h
//  Loan-ios
//
//  Created by Hao on 2018/10/29.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import <Foundation/Foundation.h>
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
// ====================登录注册====================
//获取验证码-----ok
#define LOGIN_CODE @"login/code"
//进入注册页
#define LOGIN_LINK @"login/link"
//登录-----ok
#define LOGIN_LOGIN @"login/login"
//注册
#define LOGIN_REGISTER @"login/register"
//判断是否有办卡-----ok
#define LOGIN_JUDGE @"login/judge"
//
#define LOGIN_TOKEN @"login/token"

// ====================首页====================
//获取贷款详情-----ok
#define INDEX_COMPANY @"index/company"
//获取贷款新品列表-----ok
#define INDEX_FRESH @"index/fresh"
//获取首页信息-----ok
#define INDEX_INDEX @"index/index"
//获取贷款推荐列表---ok
#define INDEX_RECOMMEND @"index/recommend"
//获取贷款攻略详情-----ok
#define INDEX_STRATEGY @"index/strategy"

// ====================贷款====================
//获取贷款产品详情---ok
#define COMPANY_DETAIL @"company/detail"
//获取贷款专区列表-----ok
#define COMPANY_INDEX @"company/index"

// ====================办卡====================
//获取全部银行-----ok
#define CREDIT_BANK_LIST @"credit/bank_list"
//获取信用卡列表-----ok
#define CREDIT_CREDIT_LIST @"credit/credit_list"
//获取银行列表-----ok
#define CREDIT_INDEX @"credit/index"

// ====================我的====================
//关于我们-----ok
#define CENTER_CONFIG @"center/config"
//意见反馈-----ok
#define CENTER_FEEDBACK @"center/feedback"
//帮助中心-----ok
#define CENTER_HELP @"center/help"
//个人中心信息-----ok
#define CENTER_INDEX @"center/index"
//我的资料-----ok
#define CENTER_INFO @"center/info"
//退出-----ok
#define CENTER_LOGOUT @"center/logout"
//修改个人信息-----ok
#define CENTER_UPDATE_INFO @"center/update_info"
//修改身份信息-----ok
#define CENTER_UPDATE_IDENTITY @"center/update_identity"
//修改资产信息-----ok
#define CENTER_UPDATE_ASSET @"center/update_asset"
//修改头像-----ok
#define CENTER_UPDATE_AVATAR @"center/update_avatar"
//上传文件-----ok
#define COMMON_FILE @"http://119.23.66.37:8989/common/file"

//=========================================
#define WE_CHAT_ID @""
#define APP_ID @"2"

// ========================================
#define isShowLog 1

@interface Constant : NSObject

@end
