//
//  BeeNet.h
//  ShopDog
//
//  Created by 陈必锋 on 2017/8/4.
//  Copyright © 2017年 shopDog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BeeNetCallback.h"
#import <AFNetworking.h>


typedef NS_ENUM(NSInteger,RequestType){
    Request_GET,
    Request_POST,
    Request_PUT,
    Request_Delete
};

@interface BeeNet : NSObject

+(instancetype)sharedInstance;

/**
 带 token 查询
 */
-(void)requestWithType:(RequestType)requestType andUrl:(NSString *)url andParam:(id)params andSuccess:(void (^)(id data))success;
/**
 带 token+fail 查询
 */
-(void)requestWithType:(RequestType)requestType url:(NSString *)url param:(id)params success:(void (^)(id data))success fail:(void (^)(NSString *message))failMsg;
/**
 带 设置请求 查询
 */
-(void)requestWithType:(RequestType)requestType andUrl:(NSString*)url andParam:(id)params andHeader:(NSDictionary*)header andRequestSerializer:(id)requestSer andResponseSerializer:(id)responseSer andSuccess:(void(^)(id data))success andFailed:(void(^)(NSString* str))failed;

/**
 默认 查询
 */
-(void)requestWithType:(RequestType)requestType andUrl:(NSString*)url andParam:(id)params andHeader:(NSDictionary*)header andSuccess:(void(^)(id data))success andFailed:(void(^)(NSString* str))failed;


-(void)requestWithType:(RequestType)requestType andUrl:(NSString*)url andParam:(id)params andHeader:(NSDictionary*)header andBeeCallBack:(BeeNetCallback*)beeCallback;


-(void)postFileWithUrl:(NSString *)url andFileKey:(NSString*)fileKey andParams:(id)param andFileData:(NSData *)FileData andHeader:(NSDictionary *)header andBeeCallback:(BeeNetCallback *)beeCallback andBlock:(void(^)(id data))blockData;


-(void)headRequest:(NSString*)url andSuccess:(void(^)(id data))success andFailed:(void(^)(NSString* str))failed;
@end
