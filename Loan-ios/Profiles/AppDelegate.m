//
//  AppDelegate.m
//  Loan-ios
//
//  Created by Hao on 2018/10/29.
//  Copyright © 2018年 Hao. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeVC.h"
#import "LoanVC.h"
#import "CreateCardVC.h"
#import "PesonalCenterVC.h"

#import "WXAPi.h"

@interface AppDelegate ()<WXApiDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    #pragma mark - IQkeyboard
    //控制自动键盘功能启用与否
    [[IQKeyboardManager sharedManager] setEnable:YES];
    //键盘弹出时，点击背景，键盘收回
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    //隐藏键盘上面的toolBar,默认是开启的
//    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:30.0f];
    
    #pragma mark - SVProgressHUD
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    
    #pragma mark - UIBarButtonItem
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14 weight:UIFontWeightLight]} forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14 weight:UIFontWeightLight]} forState:UIControlStateHighlighted];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14 weight:UIFontWeightLight]} forState:UIControlStateSelected];
    
    //是否加载办卡模块
    [self isLoadCardMode];
    
    //微信
    //还需要在Infoh-->URL Types中添加注册的微信AppId
//    [WXApi registerApp:WE_CHAT_ID];
    
    return YES;
}

- (void)isLoadCardMode {
    NSDictionary *params = @{
                             @"appId" : APP_ID
                             };
    [[BeeNet sharedInstance] requestWithType:Request_GET url:LOGIN_JUDGE param:params success:^(id data) {
        NSInteger returnCode = [NSString stringWithFormat:@"%@",data[@"code"]].integerValue;
        if (returnCode == 200) {
            BOOL isHave = [NSString stringWithFormat:@"%@",data[@"data"]].boolValue;
            
            NSLog(@"==是否显示办卡======%@",[USERDEFAULTS valueForKey:@"isHaveCardMode"]);
            [self loadBaseViewWithIsHaveCardMode:isHave];
            
        } else {
            
            [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
        }
        if(isShowLog == 1) {
            NSLog(@" -----------%@ ",data);
        }
    } fail:^(NSString *message) {
        if(isShowLog == 1) {
            NSLog(@" LOGIN_JUDGE -------%@ ",message);
        }
        [SVProgressHUD showErrorWithStatus:@"服务器正忙!"];
    }];
}

- (void)loadBaseViewWithIsHaveCardMode:(BOOL)isHaveCardMode {
    
#pragma mark - 加载tabbar
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *rootVc = [board instantiateViewControllerWithIdentifier:@"mainVC"];
    
    //homeNav
    UIStoryboard *homeBoard = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
    UINavigationController *homeNav = [homeBoard instantiateViewControllerWithIdentifier:@"homeNav"];
    
    //Loan---loanNav
    UIStoryboard *loanBoard = [UIStoryboard storyboardWithName:@"Loan" bundle:nil];
    UINavigationController *loanNav = [loanBoard instantiateViewControllerWithIdentifier:@"loanNav"];
    //CreateCard---cardNav
    UIStoryboard *cardBoard = [UIStoryboard storyboardWithName:@"CreateCard" bundle:nil];
    UINavigationController *cardNav = [cardBoard instantiateViewControllerWithIdentifier:@"cardNav"];
    //Me---meNav
    UIStoryboard *meBoard = [UIStoryboard storyboardWithName:@"Me" bundle:nil];
    UINavigationController *meNav = [meBoard instantiateViewControllerWithIdentifier:@"meNav"];
    
    if (isHaveCardMode) {
        rootVc.viewControllers = @[homeNav, loanNav, cardNav, meNav];
        [USERDEFAULTS setBool:YES forKey:@"isHaveCardMode"];
        [USERDEFAULTS synchronize];
    } else {
        rootVc.viewControllers = @[homeNav, loanNav, meNav];
        [USERDEFAULTS setBool:NO forKey:@"isHaveCardMode"];
        [USERDEFAULTS synchronize];
    }
    
    self.window.rootViewController = rootVc;
    [self.window makeKeyAndVisible];
    
}

#pragma mark ====================we chat====================


//- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
//
//    return [WXApi handleOpenURL:url delegate:self];
//
//}


//是微信终端向第三方程序发起请求，要求第三方程序响应。第三方程序响应完后必须调用sendRsp返回。在调用sendRsp返回时，会切回到微信终端程序界面。
//- (void)onReq:(BaseReq *)req {
//
//}

// 从微信分享过后点击返回应用的时候调用
//如果第三方程序向微信发送了sendReq的请求，那么onResp会被回调。sendReq请求调用后，会切到微信终端程序界面。
//- (void)onResp:(BaseResp *)resp {

//    //把返回的类型转换成与发送时相对于的返回类型,这里为SendMessageToWXResp
//    SendMessageToWXResp *sendResp = (SendMessageToWXResp *)resp;
//
//    //使用UIAlertView 显示回调信息
//    NSString *str = [NSString stringWithFormat:@"%d",sendResp.errCode];
//    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"回调信息" message:str delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil, nil];
//    [alertview show];
//    /*  WXSuccess           = 0,    /**< 成功    */
//    WXErrCodeCommon     = -1,   /**< 普通错误类型    */
//    WXErrCodeUserCancel = -2,   /**< 用户点击取消并返回    */
//    WXErrCodeSentFail   = -3,   /**< 发送失败    */
//    WXErrCodeAuthDeny   = -4,   /**< 授权失败    */
//    WXErrCodeUnsupport  = -5,   /**< 微信不支持    */
//    */
//}

//不允许横屏
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    //退出app时删除token
//    [USERDEFAULTS removeObjectForKey:@"token"];
//    [USERDEFAULTS synchronize];
    
}


@end
